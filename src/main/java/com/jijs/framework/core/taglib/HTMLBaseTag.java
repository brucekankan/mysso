package com.jijs.framework.core.taglib;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 转换编码标签
 * 
 * @author jijs
 */
public class HTMLBaseTag extends TagSupport {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2871167818814453701L;

	@Override
	public int doEndTag() throws JspException {

		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

		StringBuffer sb = new StringBuffer();
		sb.append("<base href=\"");
		sb.append(request.getScheme());
		sb.append("://");

		String serverName = request.getServerName();

		if ("0:0:0:0:0:0:0:1".equals(serverName)) {
			sb.append("localhost");
		} else {
			sb.append(serverName);
		}

		sb.append(":");
		sb.append(request.getServerPort());
		sb.append(request.getContextPath());
		sb.append("/");
		sb.append("\" />");

		JspWriter writer = null;

		try {
			writer = pageContext.getOut();
			writer.write(sb.toString());
			writer.flush();
		} catch (IOException e) {
			throw new JspException(e);
		}

		return TagSupport.EVAL_PAGE;
	}
}
