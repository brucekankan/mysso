package com.jijs.framework.core.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.dom4j.DocumentException;

import com.jijs.framework.util.IconFontHelper;

/**
 * 字体图标标签
 * 
 * @author jijs
 *
 */
public class IconFontTag extends TagSupport {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8309769155603538442L;

	private String name = null;

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int doEndTag() throws JspException {

		StringBuffer sb = new StringBuffer();

		try {
			IconFontHelper helper = IconFontHelper.getInstance();

			sb.append("<i class=\"iconfont\">");
			sb.append(helper.getUnicode(name));
			sb.append("</i>");

		} catch (DocumentException e) {
			e.printStackTrace();
		}

		JspWriter writer = null;

		try {
			writer = pageContext.getOut();
			writer.write(sb.toString());
			writer.flush();
		} catch (IOException e) {
			throw new JspException(e);
		}

		return TagSupport.EVAL_PAGE;
	}
}
