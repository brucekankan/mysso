package com.jijs.framework.core.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

/**
 * 连接中随机数的标签类
 * 
 * @author jijs
 *
 */
public class LinkRandomTag extends TagSupport {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2666055459528392635L;

	private static final String DEFAULT_PARAM_NAME = "r";

	/**
	 * 变量的名称
	 */
	private String paramName = null;

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	@Override
	public int doEndTag() throws JspException {

		paramName = StringUtils.defaultIfEmpty(paramName, DEFAULT_PARAM_NAME);

		JspWriter writer = null;

		try {
			writer = pageContext.getOut();
			writer.write(paramName + "=" + Math.random());
			writer.flush();
		} catch (IOException e) {
			throw new JspException(e);
		}

		return TagSupport.EVAL_PAGE;
	}
}
