package com.jijs.framework.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jijs.framework.CodeConstant;
import com.jijs.framework.controller.BaseController;
import com.jijs.framework.model.EasyUIDataGrid;
import com.jijs.framework.model.JsonResult;
import com.jijs.framework.model.PageHelper;
import com.jijs.framework.model.Tree;
import com.jijs.framework.model.pmodel.AdeRole;
import com.jijs.framework.model.pmodel.AdeUser;
import com.jijs.framework.service.ModuleManagerService;
import com.jijs.framework.service.RoleManagerService;
import com.jijs.framework.util.StringUtil;
import com.jijs.framework.util.UserUtil;

/**
 * 角色控制器
 * 
 * @author jijs
 * 
 */
@Controller
@RequestMapping("/admin/roleManager")
public class RoleManagerController extends BaseController {

    @Autowired
    private RoleManagerService roleService;

    @Autowired
    private ModuleManagerService moduleService;

    /**
     * 跳转到角色管理页面
     * 
     * @return
     */
    @RequestMapping("/manager")
    public String manager() {
        return "/admin/role/role";
    }

    /**
     * 跳转到角色添加页面
     * 
     * @return
     */
    @RequestMapping("/addPage")
    public String addPage(HttpServletRequest request) {
        AdeRole r = new AdeRole();
        r.setId(StringUtil.generateUUID());
        request.setAttribute("role", r);
        request.setAttribute("operate", CodeConstant.OPERATE_ADD);
        return "/admin/role/roleEdit";
    }

    /**
     * 添加角色
     * 
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public JsonResult add(AdeRole role, HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        JsonResult j = new JsonResult();

        String msg = roleService.add(role, user);
        // 校验 角色重复记录
        j.setObj(msg);
        j.setSuccess(true);
        j.setMsg("添加成功！");
        return j;
    }

    /**
     * 跳转到角色修改页面
     * 
     * @return
     */
    @RequestMapping("/editPage")
    public String editPage(HttpServletRequest request, String id) {
        AdeRole r = roleService.get(id);
        request.setAttribute("role", r);
        request.setAttribute("operate", CodeConstant.OPERATE_EDIT);
        return "/admin/role/roleEdit";
    }

    /**
     * 修改角色
     * 
     * @param role
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public JsonResult edit(AdeRole role) {
        JsonResult j = new JsonResult();
        roleService.edit(role);
        j.setSuccess(true);
        j.setMsg("编辑成功！");
        return j;
    }

    /**
     * 获得角色列表
     * 
     * @return
     */
    @RequestMapping("/dataGrid")
    @ResponseBody
    public EasyUIDataGrid dataGrid(PageHelper ph, HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        return roleService.dataGrid(user, ph);
    }

    /**
     * 角色树(只能看到自己拥有的角色)
     * 
     * @return
     */
    @RequestMapping("/tree")
    @ResponseBody
    public List<Tree> tree(HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        return roleService.tree(user);
    }

    /**
     * 角色树
     * 
     * @return
     */
    @RequestMapping("/allTree")
    @ResponseBody
    public List<Tree> allTree() {
        return roleService.allTree();
    }

    /**
     * 删除角色
     * 
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(String id) {
        JsonResult j = new JsonResult();
        roleService.delete(id);
        j.setMsg("删除成功！");
        j.setSuccess(true);
        return j;
    }

    /**
     * 跳转到角色授权页面
     * 
     * @return
     */
    @RequestMapping("/grantPage")
    public String grantPage(HttpServletRequest request, String id) {
        AdeRole r = roleService.get(id);
        request.setAttribute("role", r);
        return "/admin/role/roleGrant";
    }

    /**
     * 角色授权页面：获得模块+权限树<br>
     * 通过用户ID判断，他能看到的模块
     *
     * @param session
     * @return
     */
    @RequestMapping("/grantModuleTree")
    @ResponseBody
    public List<Tree> grantModuleTree(HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        return moduleService.treeAllType(user);
    }

    /**
     * 授权
     * 
     * @param role
     * @return
     */
    @RequestMapping("/grant")
    @ResponseBody
    public JsonResult grant(AdeRole role) {
        JsonResult j = new JsonResult();
        roleService.grant(role);
        j.setMsg("授权成功！");
        j.setSuccess(true);
        return j;
    }
}
