package com.jijs.framework.controller.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jijs.framework.controller.BaseController;
import com.jijs.framework.util.IconFontHelper;

/**
 * 
 * @author jijs
 *
 */
@Controller
@RequestMapping("/admin/iconfontManager")
public class IconfontManagerController extends BaseController {

	/** 日志对象 */
	private static final Logger LOG = Logger.getLogger(IconfontManagerController.class);

	@RequestMapping("/list")
	public String list(HttpServletRequest request) {

		try {
			IconFontHelper helper = IconFontHelper.getInstance();

			Map<String, String> icons = helper.getCacheClone();

			request.setAttribute("icons", icons);

		} catch (DocumentException e) {
			LOG.error(e.getMessage(), e);
		}

		return "/admin/iconfont/iconfont";
	}
}
