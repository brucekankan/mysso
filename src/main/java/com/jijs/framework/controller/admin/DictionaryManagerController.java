package com.jijs.framework.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jijs.framework.CodeConstant;
import com.jijs.framework.controller.BaseController;
import com.jijs.framework.model.EasyUIComboTree;
import com.jijs.framework.model.JsonResult;
import com.jijs.framework.model.pmodel.AdeDictionary;
import com.jijs.framework.service.DictionaryManagerService;
import com.jijs.framework.util.LogUtil;
import com.jijs.framework.util.StringUtil;

/**
 * 字典信息管理
 * 
 * @author jijs
 * 
 */
@Controller
@RequestMapping("/admin/dictionaryManager")
public class DictionaryManagerController extends BaseController {

    /** 日志对象 */
    private static final Logger LOG = Logger.getLogger(DictionaryManagerController.class);

    @Autowired
    private DictionaryManagerService dictionaryService = null;

    /**
     * 跳转到字典管理页面
     * 
     * @return
     */
    @RequestMapping("/manager")
    public String dictionary(HttpServletRequest request) {
        return "/admin/dictionary/dictionary";
    }

    @ResponseBody
    @RequestMapping("/dictionaryDataGrid")
    public List<AdeDictionary> dictionaryDataGrid(AdeDictionary dictionary, HttpServletRequest request,
            HttpSession session) {
        return dictionaryService.dataGrid(dictionary);
    }

    /**
     * 跳转到添加页面
     * 
     * @return
     */
    @RequestMapping("/addPage")
    public String addPage(HttpServletRequest request) {
        AdeDictionary r = new AdeDictionary();
        r.setId(StringUtil.generateUUID());
        request.setAttribute("dictionary", r);
        request.setAttribute("operate", CodeConstant.OPERATE_ADD);
        return "/admin/dictionary/dictionaryEdit";
    }

    /**
     * 添加字典
     */
    @ResponseBody
    @RequestMapping("/add")
    public JsonResult add(AdeDictionary dictionary, HttpServletRequest request) {

        // 定义easyUI Json对象
        JsonResult j = new JsonResult();

        // 如果父亲节点的ID为空字符串的话，设置为null
        dictionary.setDictPid(StringUtil.defaultIfEmpty(dictionary.getDictPid(), null));

        // 调用Service方法
        try {
            String msg = dictionaryService.add(dictionary);
            if (msg == null) {
                j.setSuccess(true);
                j.setMsg("新数据字典添加成功！");
                LogUtil.adminLogs("新数据字典添加成功", CodeConstant.ADD_OPERATION, request);
            } else {
                j.setSuccess(false);
                j.setMsg(msg);
                LogUtil.adminLogs("新数据字典添加失败", CodeConstant.ADD_OPERATION, request);
            }
        } catch (Exception e) {
            j.setSuccess(false);
            j.setMsg("操作异常，请联系系统维护人员！");
            LogUtil.adminLogs("新数据字典添加失败", CodeConstant.ADD_OPERATION, request);
            LOG.error(e.getMessage(), e);
        }

        return j;
    }

    /**
     * 跳转到字典修改页面
     * 
     * @return
     */
    @RequestMapping("/editPage")
    public String editPage(HttpServletRequest request, String id, String dictType) {
        AdeDictionary d = dictionaryService.getDictionary(id);
        request.setAttribute("dictionary", d);

        if (CodeConstant.DICT_TYPE_USER.equals(dictType)) {
            request.setAttribute("operate", "editUserDict");
        } else if (CodeConstant.DICT_TYPE_SYSTEM.equals(dictType)) {
            request.setAttribute("operate", "editSystemDict");
        }

        return "/admin/dictionary/dictionaryEdit";
    }

    /**
     * 修改字典
     */
    @ResponseBody
    @RequestMapping("/editUserDict")
    public JsonResult editUserDict(AdeDictionary dictionary, HttpServletRequest request) {
        return edit(dictionary, CodeConstant.DICT_TYPE_USER, request);
    }

    /**
     * 修改字典
     */
    @ResponseBody
    @RequestMapping("/editSystemDict")
    public JsonResult editSystemDict(AdeDictionary dictionary, HttpServletRequest request) {
        return edit(dictionary, CodeConstant.DICT_TYPE_SYSTEM, request);
    }

    /**
     * 修改字典项目
     */
    public JsonResult edit(AdeDictionary dictionary, String dictType, HttpServletRequest request) {

        // 定义easyUI Json对象
        JsonResult j = new JsonResult();

        // 如果父亲节点的ID为空字符串的话，设置为null
        dictionary.setDictPid(StringUtil.defaultIfEmpty(dictionary.getDictPid(), null));

        // 调用Service方法
        try {
            String msg = dictionaryService.edit(dictionary, dictType);
            if (msg == null) {
                j.setSuccess(true);
                j.setObj("success");
                j.setMsg("数据字典修改成功！");
                LogUtil.adminLogs("数据字典修改成功!", CodeConstant.EDIT_OPERATION, request);
            } else {
                j.setSuccess(false);
                j.setMsg(msg);
                LogUtil.adminLogs("数据字典修改失败！：", CodeConstant.EDIT_OPERATION, request);
            }
        } catch (Exception e) {
            j.setSuccess(false);
            j.setMsg("操作异常，请联系系统维护人员！");
            LogUtil.adminLogs("数据字典修改失败", CodeConstant.EDIT_OPERATION, request);
            LOG.error(e.getMessage(), e);
        }

        return j;
    }

    /**
     * 删除用户字典
     * 
     * @param String
     *            id 角色对象ID
     * @return JsonResult 消息对象
     */
    private JsonResult deleteDict(String id, String dictType, HttpServletRequest request) {

        // 定义easyUI Json对象
        JsonResult j = new JsonResult();

        // 调用Service方法
        try {
            String msg = dictionaryService.delete(id, dictType);
            if (msg == null) {
                j.setSuccess(true);
                j.setMsg("删除字典成功！");
                LogUtil.adminLogs("数据字典删除成功", CodeConstant.DELETE_OPERATION, request);

            } else {
                j.setSuccess(false);
                j.setMsg(msg);
                LogUtil.adminLogs("数据字典删除失败", CodeConstant.DELETE_OPERATION, request);
            }
        } catch (Exception e) {
            j.setSuccess(false);
            j.setMsg("操作异常，请联系系统维护人员！");
            LogUtil.adminLogs("数据字典删除失败", CodeConstant.DELETE_OPERATION, request);
            LOG.error(e.getMessage(), e);
        }

        return j;
    }

    /**
     * 删除用户字典
     * 
     * @param String
     *            id 字典项ID
     * @return JsonResult 消息对象
     */
    @RequestMapping("/deleteUserDict")
    @ResponseBody
    public JsonResult deleteUserDict(String id, HttpServletRequest request) {
        return deleteDict(id, CodeConstant.DICT_TYPE_USER, request);
    }

    /**
     * 删除系统字典
     * 
     * @param String
     *            id 字典项ID
     * @return JsonResult 消息对象
     */
    @RequestMapping("/deleteSystemDict")
    @ResponseBody
    public JsonResult deleteSystemDict(String id, HttpServletRequest request) {
        return deleteDict(id, CodeConstant.DICT_TYPE_SYSTEM, request);
    }

    /**
     * 获取数据字典列表
     * 
     * @param excapeId
     *            例外的ID，比如在编辑数据字典的时候，不要把自己也显示出来。
     * @return
     */
    @ResponseBody
    @RequestMapping("/dictionaryComboTree")
    public List<EasyUIComboTree> dictionaryComboTree(String excapeId) {
        return dictionaryService.dictionarysComboTree(excapeId);
    }

    /**
     * 根据字典类型代码获取相应的字典树信息(下拉选择代码ID的时候使用)
     * 
     * @param dictCode
     *            字典code
     * @return
     */
    @RequestMapping("/getDictsByTypeCode")
    @ResponseBody
    public List<AdeDictionary> getDictsByTypeCode(String dictCode) {
        return dictionaryService.getDictsByTypeCode(dictCode);
    }
}
