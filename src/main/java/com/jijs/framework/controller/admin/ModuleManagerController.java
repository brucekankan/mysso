package com.jijs.framework.controller.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

import com.jijs.framework.CodeConstant;
import com.jijs.framework.controller.BaseController;
import com.jijs.framework.model.JsonResult;
import com.jijs.framework.model.Tree;
import com.jijs.framework.model.pmodel.AdeModule;
import com.jijs.framework.model.pmodel.AdeUser;
import com.jijs.framework.service.IconfontManagerService;
import com.jijs.framework.service.ModuleManagerService;
import com.jijs.framework.util.StringUtil;
import com.jijs.framework.util.UserUtil;

/**
 * 模块控制器
 * 
 * @author jijs
 * 
 */
@Controller
@RequestMapping("/admin/moduleManager")
public class ModuleManagerController extends BaseController {

    @Autowired
    private ModuleManagerService moduleService;

    @Autowired
    private IconfontManagerService iconfontManagerService;

    /**
     * 获得菜单
     * 
     * 通过用户ID判断，他能看到的资源
     * 
     * @param session
     * @return
     */
    @RequestMapping("/systemMenuTree")
    @ResponseBody
    public List<Tree> systemMenuTree(HttpServletRequest request) {

        String moduleGround = StringUtil.defaultIfEmpty(request.getParameter("moduleGround"),
                CodeConstant.MODULE_GROUND_BACKGROUND);

        AdeUser user = UserUtil.getLoginUser(request);
        return moduleService.systemMenuTree(user, moduleGround);
    }

    /**
     * 获取当前用户能够看到的一级菜单列表<br>
     * 此功能用在用户登录后，打开的后台首页左侧的菜单
     * 
     * @param request
     * @return 当前用户能够看到的模块列表
     */
    @RequestMapping("/topMenu")
    @ResponseBody
    public List<Tree> topMenu(HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);

        String moduleGround = StringUtil.defaultIfEmpty(request.getParameter("moduleGround"),
                CodeConstant.MODULE_GROUND_BACKGROUND);

        return moduleService.topMenu(user, moduleGround);
    }

    /**
     * 当前用户能够看到的二级树形菜单<br>
     * 当菜单为多级的时候，则使用这个方法来返回树形菜单<br>
     * 当菜单为单级的时候，则使用这个方法返回平级的菜单
     * 
     * @param request
     * @return 当前用户能够看到的模块列表
     */
    @RequestMapping("/treeMenu")
    @ResponseBody
    public List<Tree> treeMenu(HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        String pid = request.getParameter("pid");

        String moduleGround = StringUtil.defaultIfEmpty(request.getParameter("moduleGround"),
                CodeConstant.MODULE_GROUND_BACKGROUND);

        return moduleService.treeMenu(user, pid, moduleGround);
    }

    /**
     * 获得模块树<br>
     * 用来生成下拉框
     *
     * @param request
     * @return
     */
    @RequestMapping("/comboTree")
    @ResponseBody
    public List<Tree> comboTree(HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        String moduleGround = request.getParameter("moduleGround");
        return moduleService.tree(user, moduleGround);
    }

    /**
     * 跳转到模块管理页面
     * 
     * @return
     */
    @RequestMapping("/manager")
    public String manager() {
        return "/admin/module/module";
    }

    /**
     * 模块的TreeGrid<br>
     * <p>
     * 使用:
     * <ul>
     * <li>模块管理中的模块TreeGrid</li>
     * <li>权限管理中左侧的模块TreeGrid</li>
     * </ul>
     * </p>
     * 
     * @return
     */
    @RequestMapping("/treeGrid")
    @ResponseBody
    public List<AdeModule> treeGrid(HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        return moduleService.treeGrid(user);
    }

    /**
     * 跳转到权限管理页面
     * 
     * @return
     */
    @RequestMapping("/permission")
    public String permission() {
        return "/admin/module/permission";
    }

    /**
     * 获得菜单的功能项
     * 
     * 
     * @return
     */
    @RequestMapping("/dataGridForPermissions")
    @ResponseBody
    public List<AdeModule> dataGridForPermissions(HttpServletRequest request, String pid) {
        if (StringUtils.isBlank(pid)) {
            return new ArrayList<AdeModule>();
        }
        AdeUser user = UserUtil.getLoginUser(request);
        return moduleService.dataGridForMenu(user, pid);
    }

    /**
     * 跳转到模块添加页面
     * 
     * @return
     */

    @RequestMapping("/addPage")
    public String addPage(HttpServletRequest request) {
        AdeModule r = new AdeModule();
        r.setId(StringUtil.generateUUID());
        r.setModuleGround(CodeConstant.MODULE_GROUND_BACKGROUND);// 这样在画面上就可以直接选择上级资源了
        request.setAttribute("module", r);
        request.setAttribute("operate", CodeConstant.OPERATE_ADD);

        request.setAttribute("icons", iconfontManagerService.list());// 设置系统的图标

        return "/admin/module/moduleEdit";
    }

    /**
     * 跳转到模块添加页面
     * 
     * @return
     */
    @RequestMapping("/addPermissionPage")
    public String addPermissionPage(HttpServletRequest request) {

        AdeModule r = new AdeModule();
        r.setId(StringUtil.generateUUID());
        r.setModuleGround(CodeConstant.MODULE_GROUND_BACKGROUND);// 这样在画面上就可以直接选择上级资源了

        // 设置父节点
        AdeModule p = moduleService.get(request.getParameter("pid"));
        if (p != null) {
            r.setParentModuleId(p.getId());
            r.setParentModuleName(p.getModuleName());
        }

        request.setAttribute("module", r);
        request.setAttribute("operate", CodeConstant.OPERATE_ADD);
        return "/admin/module/permissionEdit";
    }

    /**
     * 添加模块
     * 
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public JsonResult add(AdeModule module, HttpServletRequest request) {
        AdeUser user = UserUtil.getLoginUser(request);
        JsonResult j = new JsonResult();

        // 如果输入特殊字符的话，不进行转义
        if (StringUtil.isNotEmpty(module.getModuleIcon())) {
            module.setModuleIcon(HtmlUtils.htmlUnescape(module.getModuleIcon()));
        }

        moduleService.add(module, user);
        j.setSuccess(true);
        j.setMsg("添加成功！");
        return j;
    }

    /**
     * 跳转到模块编辑页面
     * 
     * @return
     */
    @RequestMapping("/editPage")
    public String editPage(HttpServletRequest request, String id) {
        AdeModule r = moduleService.get(id);
        request.setAttribute("module", r);
        request.setAttribute("operate", CodeConstant.OPERATE_EDIT);

        request.setAttribute("icons", iconfontManagerService.list());// 设置系统的图标

        return "/admin/module/moduleEdit";
    }

    /**
     * 跳转到模块添加页面
     * 
     * @return
     */
    @RequestMapping("/editPermissionPage")
    public String editPermissionPage(HttpServletRequest request, String id) {

        AdeModule r = moduleService.get(id);
        r.setModuleGround(CodeConstant.MODULE_GROUND_BACKGROUND);// 这样在画面上就可以直接选择上级资源了

        request.setAttribute("module", r);
        request.setAttribute("operate", CodeConstant.OPERATE_EDIT);
        return "/admin/module/permissionEdit";
    }

    /**
     * 编辑模块
     * 
     * @param module
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public JsonResult edit(AdeModule module) {

        // 如果输入特殊字符的话，不进行转义
        if (StringUtil.isNotEmpty(module.getModuleIcon())) {
            module.setModuleIcon(HtmlUtils.htmlUnescape(module.getModuleIcon()));
        }

        JsonResult j = new JsonResult();
        moduleService.edit(module);
        j.setSuccess(true);
        j.setMsg("编辑成功！");
        return j;
    }

    /**
     * 删除模块
     * 
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(String id) {
        JsonResult j = new JsonResult();

        String message = moduleService.delete(id);

        if (message == null) {
            j.setMsg("删除成功！");
            j.setSuccess(true);
        } else {
            j.setMsg(message);
            j.setSuccess(false);
        }

        return j;
    }
}
