package com.jijs.framework.controller.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jijs.framework.service.SystemInfoService;

/**
 * 系统信息控制器
 * 
 * @author jijs
 * 
 */
@Controller
@RequestMapping("/admin/systemInfo")
public class SystemInfoController {

    @Autowired
    private SystemInfoService systemInfoService;

    /**
     * 获取系统相关信息
     */
    @RequestMapping("/systemInfo")
    public String systemInfo(HttpServletRequest request) {

        Map<String, String> runtimeInfos = systemInfoService.getRuntimeInfo(request);
        request.setAttribute("runtimeInfos", runtimeInfos);

        Map<String, String> serverInfos = systemInfoService.getServerInfo(request);
        request.setAttribute("serverInfos", serverInfos);

        Map<String, String> jdkVmInfos = systemInfoService.getJdkVmInfo(request);
        request.setAttribute("jdkVmInfos", jdkVmInfos);

        return "/admin/systemInfo";
    }
}
