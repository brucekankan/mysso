package com.jijs.framework.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jijs.framework.CodeConstant;
import com.jijs.framework.controller.BaseController;
import com.jijs.framework.model.JsonResult;
import com.jijs.framework.model.Tree;
import com.jijs.framework.model.pmodel.AdeOrganization;
import com.jijs.framework.service.OrganizationManagerService;
import com.jijs.framework.util.LogUtil;
import com.jijs.framework.util.StringUtil;

/**
 * 该模块主要为了展示图表例子，方便其他程序员学习使用
 * 
 * @author jijs
 */

@Controller
@RequestMapping("/admin/organizationManager")
public class OrganizationManagerController extends BaseController {

	@Autowired
	private OrganizationManagerService organizationService;

	/**
	 * 跳转到图表页面
	 * 
	 * @return
	 */
	@RequestMapping("/treeGrid")
	public String treeGrid() {
		return "/admin/organization/organization";
	}

	/**
	 * 获取组织机构list
	 * 
	 * @author Guodong 2015-06-11 15:56:03
	 * @param navigation
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/loadTreeGrid")
	public List<AdeOrganization> loadTreeGrid(AdeOrganization organization, HttpServletRequest request,
			HttpSession session) {
		return organizationService.organizationTreeGrid(organization);
	}

	/**
	 * 跳转到添加页面
	 * 
	 * @return
	 */
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		request.setAttribute("operate", CodeConstant.OPERATE_ADD);
		return "/admin/organization/organizationEdit";
	}

	/**
	 * 添加字典
	 * 
	 * @author Guodong 2015-06-11 19:20:21
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public JsonResult add(AdeOrganization org, HttpServletRequest request) {

		// 定义easyUI Json对象
		JsonResult j = new JsonResult();

		if (org == null) {
			j.setSuccess(false);
			j.setMsg("请填写组织机构信息！");
			return j;
		}

		// 调用Service方法
		try {
			org.setId(StringUtil.generateUUID());
			String msg = organizationService.add(org);
			if (msg == null) {
				j.setSuccess(true);
				j.setMsg("新组织机构添加成功！");
				LogUtil.adminLogs("新组织机构添加成功。", CodeConstant.ADD_OPERATION, request);

			} else {
				j.setSuccess(false);
				j.setMsg(msg);
				LogUtil.adminLogs("新组织机构添加失败。", CodeConstant.ADD_OPERATION, request);
			}
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("操作异常，请联系系统维护人员！");
			LogUtil.adminLogs("新组织机构添加失败。", CodeConstant.ADD_OPERATION, request);
			e.printStackTrace();
		}

		return j;
	}

	@ResponseBody
	@RequestMapping("/organizationComboTree")
	public List<Tree> organizationComboTree() {
		return organizationService.organizationComboTree();
	}

	@RequestMapping("/delete")
	@ResponseBody
	public JsonResult delete(String id, HttpServletRequest request) {

		// 定义easyUI Json对象
		JsonResult j = new JsonResult();

		// 调用Service方法
		try {

			String msg = organizationService.deleteOrganization(id);
			if (msg == null) {
				j.setSuccess(true);
				j.setMsg("删除组织机构成功！");
				LogUtil.adminLogs("新组织机构删除成功", CodeConstant.DELETE_OPERATION, request);

			} else {
				j.setSuccess(false);
				j.setMsg(msg);
				LogUtil.adminLogs("删除成功", CodeConstant.DELETE_OPERATION, request);
			}
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("操作异常，请联系系统维护人员！");
			LogUtil.adminLogs("新组织机构删除失败", CodeConstant.DELETE_OPERATION, request);
		}

		return j;
	}

	@RequestMapping("/editPage")
	public String editPage(HttpServletRequest request, String id) {
		AdeOrganization d = organizationService.getOrganization(id);
		request.setAttribute("organization", d);
		request.setAttribute("operate", CodeConstant.OPERATE_EDIT);
		return "/admin/organization/organizationEdit";
	}

	/**
	 * 修改字典
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public JsonResult edit(AdeOrganization organization, HttpServletRequest request) {

		// 定义easyUI Json对象
		JsonResult j = new JsonResult();

		if (organization == null) {

			j.setSuccess(false);
			j.setMsg("保存对象为空，请重新操作！");
			LogUtil.adminLogs("组织机构修改失败", CodeConstant.EDIT_OPERATION, request);
			return j;
		}
		// 调用Service方法
		try {
			String msg = organizationService.edit(organization);
			if (msg == null) {
				j.setSuccess(true);
				j.setMsg("组织机构修改成功！");
				LogUtil.adminLogs("组织机构修改成功：", CodeConstant.EDIT_OPERATION, request);

			} else {
				j.setSuccess(false);
				j.setMsg(msg);
				LogUtil.adminLogs("组织机构修改失败：" + msg, CodeConstant.EDIT_OPERATION, request);
			}

		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("操作异常，请联系系统维护人员！");
			LogUtil.adminLogs("组织机构修改失败", CodeConstant.EDIT_OPERATION, request);
		}

		return j;
	}

}
