package com.jijs.framework;

/**
 * 代码常量类
 * 
 * @author jijs
 * 
 */
public class CodeConstant {

    /**
     * 超级管理员登陆用户名
     */
    public static final String SUPER_USER_LOGIN_NAME = "admin";

    /** 菜单类型：菜单 */
    public static final String MODULE_TYPE_MENU = "0";
    /** 菜单类型：权限 */
    public static final String MODULE_TYPE_PERMISSION = "1";

    /** MODULE维度：后台 */
    public static final String MODULE_GROUND_BACKGROUND = "1";
    /** MODULE维度：前台 */
    public static final String MODULE_GROUND_FOREGROUND = "2";

    /** 字典类型：用户字典 */
    public static final String DICT_TYPE_USER = "1";
    /** 字典类型：系统字典 */
    public static final String DICT_TYPE_SYSTEM = "8";

    /** 编辑页面的类型：增加 */
    public static final String OPERATE_ADD = "add";
    /** 编辑页面的类型：编辑 */
    public static final String OPERATE_EDIT = "edit";

    /** 状态：正常 */
    public static final String STATE_NORMAL = "1";
    /** 状态：冻结，逻辑删除 */
    public static final String STATE_FREEZE = "9";

    public static final String AJAX_RESULT_FAIL = "0";// 失败
    public static final String AJAX_RESULT_SUCCESS = "1";// 失败
    public static final String ACCESABLE_IMAGE_TYPE = "1";// 失败

    /** 非内置角色标识 */
    public static final Integer ROLE_ISNOT_BUILDIN = 0;

    /** 内置角色标识 */
    public static final Integer ROLE_IS_BUILDIN = 1;

    public static final String EXPORT_OPERATION = "导出";// 导出
    public static final String ADD_OPERATION = "添加";// 增加操作
    public static final String SELECT_OPERATION = "查询";// 查询操作
    public static final String DELETE_OPERATION = "删除";// 删除操作
    public static final String DROP_FILE_OPERATION = "文件删除";// 删除操作
    public static final String FILE_UPLOAD_OPERATION = "文件上传操作";// 删除操作
    public static final String EDIT_OPERATION = "编辑";// 修改操作
    public static final String CONNECT_OPERATION = "远程连接";// 连接
}