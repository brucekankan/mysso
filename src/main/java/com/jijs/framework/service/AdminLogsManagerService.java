package com.jijs.framework.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jijs.framework.dao.BaseDaoI;
import com.jijs.framework.model.EasyUIDataGrid;
import com.jijs.framework.model.PageHelper;
import com.jijs.framework.model.pmodel.AdeAdminLogs;
import com.jijs.framework.model.pmodel.AdeUser;
import com.jijs.framework.model.tmodel.TadeAdminLogs;
import com.jijs.framework.model.tmodel.TadeUser;
import com.jijs.framework.util.StringUtil;
import com.jijs.framework.util.UserUtil;

/**
 * 管理员日志类
 * 
 * @author jijs
 * 
 */
@Service
public class AdminLogsManagerService {

	@Autowired
	private BaseDaoI<TadeAdminLogs> adminLogsDao;
	@Autowired
	private BaseDaoI<TadeUser> userDao;

	public EasyUIDataGrid dataGrid(AdeAdminLogs logInfoSys, PageHelper ph, String ids) {

		EasyUIDataGrid dg = new EasyUIDataGrid();

		List<AdeAdminLogs> ul = new ArrayList<AdeAdminLogs>();

		Map<String, Object> params = new HashMap<String, Object>();

		String hql = " from TadeAdminLogs t where 1=1 ";
		
		List<TadeAdminLogs> l = adminLogsDao.find(hql + whereHql(logInfoSys, params) + orderHql(ph), params,
				ph.getPage(), ph.getRows());
		if (l != null && l.size() > 0) {
			for (TadeAdminLogs t : l) {
				AdeAdminLogs d = new AdeAdminLogs();
				BeanUtils.copyProperties(t, d);

				d.setUserName(userDao.get(TadeUser.class, t.getTadeUser().getId()).getLoginName());
				ul.add(d);
			}
		}
		dg.setRows(ul);
		dg.setTotal(adminLogsDao.count("select count(*) " + hql + whereHql(logInfoSys, params), params));
		return dg;
	}

	/**
	 * 添加至管理员日志
	 * 
	 * @param logInfoSys
	 * @param request
	 * @throws Exception
	 */
	public void add(AdeAdminLogs logInfoSys, HttpServletRequest request) throws Exception {
		TadeAdminLogs t = new TadeAdminLogs();
		AdeUser user = UserUtil.getLoginUser(request);
		// 获得当前IP地址
		BeanUtils.copyProperties(logInfoSys, t);
		if (logInfoSys.getId() == null) {
			t.setId(StringUtil.generateUUID());
			t.setOperateTime(new Date());

			TadeUser tuser = new TadeUser();
			tuser.setId(user.getId());
			t.setTadeUser(tuser);

			adminLogsDao.save(t);
		}
	}

	public EasyUIDataGrid getAdminLogs(AdeAdminLogs adminLogs, PageHelper ph) {

		EasyUIDataGrid dg = new EasyUIDataGrid();
		List<AdeAdminLogs> userLogsList = new ArrayList<AdeAdminLogs>();
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer();
		hql.append(" from TadeAdminLogs t ");
		hql.append(whereHql(adminLogs, params));
		hql.append(orderHql(ph));
		List<TadeAdminLogs> list = adminLogsDao.find(hql.toString(), params, ph.getPage(), ph.getRows());
		if (list != null) {
			for (TadeAdminLogs t : list) {
				AdeAdminLogs u = new AdeAdminLogs();
				u.setUserName(userDao.get(TadeUser.class, t.getTadeUser().getId()).getLoginName());
				BeanUtils.copyProperties(t, u);
				userLogsList.add(u);
			}
		}
		dg.setRows(userLogsList);
		dg.setTotal(adminLogsDao.count("select count(*) from TadeAdminLogs t " + whereHql(adminLogs, params), params));
		return dg;
	}

	/**
	 * 拼接的where语句
	 * 
	 * @param dingml
	 * @param params
	 * @return
	 */
	private String whereHql(AdeAdminLogs userLogs, Map<String, Object> params) {

		StringBuffer hql = new StringBuffer();
		// if (userLogs != null) {
		if (!StringUtil.isEmpty(userLogs.getUserId())) {
			TadeUser t = userDao.get(TadeUser.class, userLogs.getUserId());
			hql.append(" and t.tadeUser.loginName =:tuser");
			params.put("tuser", t.getLoginName());
		}
		if (!StringUtil.isEmpty(userLogs.getLogType())) {
			hql.append(" and t.logType = :logType ");
			params.put("logType", userLogs.getLogType());
		}
		return hql.toString();
	}

	/**
	 * 拼接的order语句
	 * 
	 * @param dingml
	 * @param params
	 * @return
	 */
	private String orderHql(PageHelper ph) {

		String orderString = "";
		if (ph.getSort() != null && ph.getOrder() != null) {
			if("userName".equals(ph.getSort())) {
				orderString = " order by t.tadeUser.userName " + ph.getOrder();
			}else {
				orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
			}
		}
		return orderString;
	}

	public void log(AdeAdminLogs adminLogs) {

		if (adminLogs.getContent().length() > 33) {
			adminLogs.setContent(StringUtils.substring(adminLogs.getContent(), 0, 32));
		}

		TadeAdminLogs t = new TadeAdminLogs();
		BeanUtils.copyProperties(adminLogs, t);
		t.setOperateTime(new Date());
		// t.setUserId(adminLogs.getContent());
		t.setId(StringUtil.generateUUID());
		adminLogsDao.save(t);
	}
}
