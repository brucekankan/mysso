package com.jijs.framework.service;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.jijs.framework.util.DateUtil;
import com.jijs.framework.util.FileUtil;

/**
 * 服务器信息
 * 
 * @author jijs
 *
 */
@Service
public class SystemInfoService {

    /**
     * 获取服务器相关信息
     * 
     * @return
     */

    public Map<String, String> getServerInfo(HttpServletRequest request) {

        ServletContext application = request.getSession().getServletContext();

        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put("服务器名称", request.getServerName());

        try {
            result.put("域名/IP", request.getServerName() + "+" + InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            result.put("域名/IP", request.getServerName());
        }

        result.put("服务器端口", String.valueOf(request.getServerPort()));
        result.put("客户端端口", String.valueOf(request.getRemotePort()));
        result.put("客户端IP", request.getRemoteAddr());
        result.put("Web 服务器", application.getServerInfo());
        result.put("操作系统", System.getProperty("os.name") + " " + System.getProperty("sun.os.patch.level") + " Ver:"
                + System.getProperty("os.version"));
        result.put("CPU核心数", String.valueOf(Runtime.getRuntime().availableProcessors()));

        try {
            @SuppressWarnings("restriction")
            com.sun.management.OperatingSystemMXBean osmxb = (com.sun.management.OperatingSystemMXBean) ManagementFactory
                    .getOperatingSystemMXBean();
            @SuppressWarnings("restriction")
            long total = osmxb.getTotalPhysicalMemorySize();
            result.put("总物理内存", FileUtil.getFileSize(total));

        } catch (Exception e) {
            result.put("总物理内存", "当前运行环境不支持计算");
        }

        // 总的物理内存
        // long totalMemorySize = osmxb.getTotalPhysicalMemorySize() / kb;

        result.put("服务器时间", DateUtil.dateFormat(new Date(), "yyyy-MM-dd HH:mm:ss"));
        result.put("CPU 信息", System.getProperty("os.arch"));
        result.put("磁盘分区", getDrivers());
        result.put("用户当前工作目录", System.getProperty("user.dir"));
        result.put("运行根路径", application.getRealPath("/"));
        return result;
    }

    private String getDrivers() {
        StringBuffer sb = new StringBuffer("");
        java.io.File[] roots = java.io.File.listRoots();
        for (int i = 0; i < roots.length; i++) {
            sb.append(roots[i] + " ");
        }
        return sb.toString();
    }

    /**
     * 获取JDK虚拟机相关的信息
     * 
     * @param request
     * @return
     */
    public Map<String, String> getJdkVmInfo(HttpServletRequest request) {

        ServletContext application = request.getSession().getServletContext();

        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put("JDK 版本", System.getProperty("java.version"));
        result.put("Servlet 版本", application.getMajorVersion() + "." + application.getMinorVersion());
        result.put("JDK 安装路径", System.getProperty("java.home"));
        result.put("编码", System.getProperty("file.encoding"));
        result.put("JAVA类路径", System.getProperty("java.class.path"));
        return result;
    }

    /**
     * 系统运行情况
     * 
     * @param request
     * @return
     */
    public Map<String, String> getRuntimeInfo(HttpServletRequest request) {

        Map<String, String> result = new LinkedHashMap<String, String>();

        result.put("最大可用内存(虚拟机)", FileUtil.getFileSize(Runtime.getRuntime().maxMemory()));
        result.put("当前使用内存(虚拟机)", FileUtil.getFileSize(Runtime.getRuntime().totalMemory()));
        result.put("当前空闲内存(虚拟机)", FileUtil.getFileSize(Runtime.getRuntime().freeMemory()));

        try {
            @SuppressWarnings("restriction")
            com.sun.management.OperatingSystemMXBean osmxb = (com.sun.management.OperatingSystemMXBean) ManagementFactory
                    .getOperatingSystemMXBean();

            @SuppressWarnings("restriction")
            long total = osmxb.getTotalPhysicalMemorySize();
            @SuppressWarnings("restriction")
            long free = osmxb.getFreePhysicalMemorySize();

            result.put("已用内存(物理内存)", FileUtil.getFileSize(total - free));
            result.put("空闲内存(物理内存)", FileUtil.getFileSize(free));

        } catch (Exception e) {
            result.put("已用内存(物理内存)", "当前运行环境不支持计算");
            result.put("空闲内存(物理内存)", "当前运行环境不支持计算");
        }

        // 获得线程总数
        ThreadGroup parentThread = Thread.currentThread().getThreadGroup();
        while(parentThread.getParent() != null) {
        	parentThread = parentThread.getParent();
        }

        result.put("启动线程总数", String.valueOf(parentThread.activeCount()));

        return result;
    }
}
