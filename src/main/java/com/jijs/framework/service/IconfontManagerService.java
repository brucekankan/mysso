package com.jijs.framework.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Service;

import com.jijs.framework.util.IconFontHelper;

/**
 * 图标服务类
 * 
 * @author jijs
 *
 */
@Service
public class IconfontManagerService {

	/** 日志对象 */
	private static final Logger LOG = Logger.getLogger(IconfontManagerService.class);

	/**
	 * 列出所有的图标
	 * 
	 * @return
	 */
	public Map<String, String> list() {

		try {
			IconFontHelper helper = IconFontHelper.getInstance();
			Map<String, String> icons = helper.getCacheClone();
			return icons;
		} catch (DocumentException e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}
}
