package com.jijs.framework.util;

import java.util.ResourceBundle;

/**
 * 项目参数工具类
 * 
 * @author jijs
 * 
 */
public class ConfigUtil {

	private static final ResourceBundle BUNDLE = java.util.ResourceBundle.getBundle("config");

	// /**
	// * 获得sessionInfo名字
	// *
	// * @return
	// */
	// public static final String getSessionInfoName() {
	// return bundle.getString("sessionInfoName");
	// }
	//
	// /**
	// * 获得sessionInfo名字
	// *
	// * @return
	// */
	// public static final String getPortalSessionInfoName() {
	// return bundle.getString("portalSessionInfoName");
	// }

	/**
	 * 通过键获取值
	 * 
	 * @param key
	 * @return
	 */
	public static final String get(String key) {
		return BUNDLE.getString(key);
	}

}
