package com.jijs.framework.util;

import javax.servlet.http.HttpServletRequest;

import com.jijs.framework.model.pmodel.AdeUser;

/**
 * 用户工具类 获得或设置登录的用户
 * 
 * @author jijs
 * 
 */
public class UserUtil {

	/**
	 * 用户在session中保存使用的key值
	 */
	public static final String LOGIN_USER = "LOGIN_USER";

	/**
	 * 获得登录的用户，如果用户为空，则返回null
	 * 
	 * @param request
	 *            请求
	 * @return 当前登录的用户
	 */
	public static AdeUser getLoginUser(HttpServletRequest request) {
		return (AdeUser) request.getSession().getAttribute(LOGIN_USER);
	}

	/**
	 * 设置登录的用户，除了登录，请不要随意调用这个方法
	 * 
	 * @param request
	 *            请求
	 * @param user
	 *            当前登录的用户
	 */
	public static void setLoginUser(HttpServletRequest request, AdeUser user) {
		request.getSession().setAttribute(LOGIN_USER, user);
	}
}
