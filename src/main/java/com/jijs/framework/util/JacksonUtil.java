package com.jijs.framework.util;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * jackson工具类，可以将对象转换为json对象
 * 
 * @author jijs
 *
 */
public final class JacksonUtil {

	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
	}

	public static String toJSONString(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			return "";
		}
	}

}
