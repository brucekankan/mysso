package com.jijs.framework.util;

import javax.servlet.http.HttpServletRequest;

import com.jijs.framework.model.pmodel.AdeAdminLogs;
import com.jijs.framework.model.pmodel.AdeUser;
import com.jijs.framework.model.pmodel.AdeUserLogs;
import com.jijs.framework.service.AdminLogsManagerService;
import com.jijs.framework.service.UserLogsManagerService;

/**
 * 日志工具类
 * 
 * @author jijs
 * 
 */
public class LogUtil {

	/**
	 * @author 赖星其
	 * 
	 * @date 2015年11月26日 下午3:55:56
	 * @param message
	 *            记录信息
	 * @param logType
	 *            日志类型
	 * @param recordId
	 *            记录ID
	 * @param tableName
	 *            表名称
	 * @param request
	 *            请求
	 */
	public static void adminLogs(String message, String logType, HttpServletRequest request) {
		AdminLogsManagerService newsLogInfoSysService = (AdminLogsManagerService) SpringBeanUtil
				.getBean("adminLogsManagerService");
		try {
			AdeAdminLogs log = new AdeAdminLogs();
			AdeUser user = UserUtil.getLoginUser(request);
			log.setContent(message);
			log.setBrowserType(UserAgentUtil.getBrowserType(request));
			log.setBrowserVersion(UserAgentUtil.getBrowserVersion(request));
			log.setOsType(UserAgentUtil.getOSType(request));
			log.setOsVersion(UserAgentUtil.getOSVersion(request));
			log.setIp(IpUtil.getIpAddr(request));
			log.setUserId(user.getId());
			log.setLogType(logType);
			newsLogInfoSysService.add(log, request);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * 添加至用户日志
	 * 
	 * @param message
	 * @param logType
	 * @param request
	 */
	public static void userLogs(String message, String logType, HttpServletRequest request) {
		UserLogsManagerService newsLogInfoUserService = (UserLogsManagerService) SpringBeanUtil
				.getBean("userLogsManagerService");
		try {
			AdeUserLogs log = new AdeUserLogs();
			AdeUser user = UserUtil.getLoginUser(request);
			log.setContent(message);
			log.setBrowserType(UserAgentUtil.getBrowserType(request));
			log.setBrowserVersion(UserAgentUtil.getBrowserVersion(request));
			log.setOsType(UserAgentUtil.getOSType(request));
			log.setOsVersion(UserAgentUtil.getOSVersion(request));
			log.setIp(IpUtil.getIpAddr(request));
			log.setUserId(user.getId());
			log.setLogType(logType);
			newsLogInfoUserService.add(log);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
