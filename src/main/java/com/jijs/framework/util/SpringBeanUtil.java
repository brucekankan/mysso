package com.jijs.framework.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * spring的上下文环境工具类，可以获得spring中定义的对象
 * 
 * @author jijs
 *
 */
public class SpringBeanUtil implements ApplicationContextAware {

	private static ApplicationContext applicationContext = null;

	/**
	 * 根据 Bean 的 name 获取 Bean 实例
	 * 
	 * @param beanName
	 * @return
	 */
	public static Object getBean(String beanName) {
		return applicationContext.getBean(beanName);
	}

	/**
	 * 通过 Bean 的 Class 获取 Bean 实例
	 * 只适合一个class只被定义一次的bean（也就是说，根据class不能匹配出多个该class的实例）
	 * 
	 * @param clazz
	 * @return
	 */
	public static Object getBean(Class<?> clazz) {
		if (clazz == null || applicationContext == null) {
			return null;
		}
		return applicationContext.getBean(clazz);
	}

	/**
	 * 获取 applicationContext 中所有的 Bean 的 Name
	 * 
	 * @return
	 */
	public static String[] getBeanDefinitionNames() {
		return applicationContext.getBeanDefinitionNames();
	}

	@Override
	public void setApplicationContext(ApplicationContext content) throws BeansException {
		SpringBeanUtil.applicationContext = content;
	}

}
