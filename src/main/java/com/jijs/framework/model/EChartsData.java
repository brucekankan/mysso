package com.jijs.framework.model;

import java.util.ArrayList;
import java.util.List;

/**
 * ECharts类
 * @author wuax
 *
 */
public class EChartsData {
	
	public List<String> legend = new ArrayList<String>();//数据分组  图例
    public List<String> category = new ArrayList<String>();//横坐标  
    public List<Series> series = new ArrayList<Series>();//纵坐标  
    
	public EChartsData(List<String> legend, List<String> category, List<Series> series) {
		super();
		this.legend = legend;
		this.category = category;
		this.series = series;
	}
      
      
    

}
