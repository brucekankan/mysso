package com.jijs.framework.model.pmodel;

public class AdeDictionary implements java.io.Serializable {

    private static final long serialVersionUID = 1787149007512702094L;

    /** id编码 */
    private String id;
    /** 字典名称 */
    private String dictName;
    /** 字典code值 */
    private String dictCode;
    /** 数据字典类型，1：系统字典，8：用户字典 */
    private String dictType;
    /** 字典排序 */
    private Integer dictSort;
    /** 字典描述信息 */
    private String dictDesc;
    /** 是否使用，1：使用，9：不使用 */
    private String dictStatus;
    /** 父节点ID编码 */
    private String dictPid;
    /** 数据字典的值，可以用来组成配置项 */
    private String dictValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public Integer getDictSort() {
        return dictSort;
    }

    public void setDictSort(Integer dictSort) {
        this.dictSort = dictSort;
    }

    public String getDictDesc() {
        return dictDesc;
    }

    public void setDictDesc(String dictDesc) {
        this.dictDesc = dictDesc;
    }

    public String getDictStatus() {
        return dictStatus;
    }

    public void setDictStatus(String dictStatus) {
        this.dictStatus = dictStatus;
    }

    public String getDictPid() {
        return dictPid;
    }

    public void setDictPid(String dictPid) {
        this.dictPid = dictPid;
    }

    public String getDictValue() {
        return dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }

}
