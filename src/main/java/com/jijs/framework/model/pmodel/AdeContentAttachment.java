package com.jijs.framework.model.pmodel;

import java.util.Date;

public class AdeContentAttachment implements java.io.Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 318784647208408165L;

    /***/
    private String id;
    /***/
    private String tdemoContentId;
    /** 附件名称 */
    private String name;
    /** 附件url */
    private String link;
    /** 附件大小 */
    private String size;
    /** 排序号 */
    private Integer orderNum;
    /** 上传时间 */
    private Date createTime;
    /** 上传人 */
    private String createUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTdemoContentId() {
        return tdemoContentId;
    }

    public void setTdemoContentId(String tdemoContentId) {
        this.tdemoContentId = tdemoContentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

}
