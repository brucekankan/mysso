package com.jijs.framework.model.pmodel;

/**
 * 
 * Organization generated by 项目开发部
 */

public class AdeOrganization implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/** id编码 */
	private String id;
	/** 组织机构名称 */
	private String orgName;
	/** 组织机构地址 */
	private String orgAddress;
	/** 组织机构负责人 */
	private String orgLeader;
	/** 联系电话 */
	private String orgPhone;
	/** 详细描述 */
	private String orgDesc;
	/** 上级机构ID编码 */
	private String orgPid;
	/** 是否使用 1：使用 0 废弃 */
	private String orgStatus;
	/***/
	private Integer orgSort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgAddress() {
		return orgAddress;
	}

	public void setOrgAddress(String orgAddress) {
		this.orgAddress = orgAddress;
	}

	public String getOrgLeader() {
		return orgLeader;
	}

	public void setOrgLeader(String orgLeader) {
		this.orgLeader = orgLeader;
	}

	public String getOrgPhone() {
		return orgPhone;
	}

	public void setOrgPhone(String orgPhone) {
		this.orgPhone = orgPhone;
	}

	public String getOrgDesc() {
		return orgDesc;
	}

	public void setOrgDesc(String orgDesc) {
		this.orgDesc = orgDesc;
	}

	public String getOrgPid() {
		return orgPid;
	}

	public void setOrgPid(String orgPid) {
		this.orgPid = orgPid;
	}

	public String getOrgStatus() {
		return orgStatus;
	}

	public void setOrgStatus(String orgStatus) {
		this.orgStatus = orgStatus;
	}

	public Integer getOrgSort() {
		return orgSort;
	}

	public void setOrgSort(Integer orgSort) {
		this.orgSort = orgSort;
	}

}
