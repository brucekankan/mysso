package com.jijs.framework.model.pmodel;

public class AdeRole implements java.io.Serializable {

    private static final long serialVersionUID = 4252460673686347867L;
    private String id;
    private String roleName;
    private String roleDesc;
    private Integer roleSort;
    private String iconCls;
    // 新添加的字段是否内置角色
    private Integer roleIsBuildin;

    private String moduleIds;
    private String moduleNames;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public Integer getRoleSort() {
        return roleSort;
    }

    public void setRoleSort(Integer roleSort) {
        this.roleSort = roleSort;
    }

    public String getIconCls() {
        return iconCls;
    }

    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }

    public Integer getRoleIsBuildin() {
        return roleIsBuildin;
    }

    public void setRoleIsBuildin(Integer roleIsBuildin) {
        this.roleIsBuildin = roleIsBuildin;
    }

    public String getModuleIds() {
        return moduleIds;
    }

    public void setModuleIds(String moduleIds) {
        this.moduleIds = moduleIds;
    }

    public String getModuleNames() {
        return moduleNames;
    }

    public void setModuleNames(String moduleNames) {
        this.moduleNames = moduleNames;
    }

}
