package com.jijs.framework.model.pmodel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jijs.framework.model.UploadFileModel;

public class AdeContent implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /***/
    private String id;
    /** 标题 */
    private String title;
    /** 副标题 */
    private String subTitle;
    /** 文章作者 */
    private String author;
    /** 文章来源 */
    private String source;
    /** 是否为外链，0：非外链，1：外链 */
    private String isOutLink;
    /** 文章URL */
    private String link;
    /** 发布时间 */
    private Date publishTime;
    private String publishTimeStr;

    /** 状态。0：未发布，1：发布，9：删除 */
    private String status;
    /** 文章内容 */
    private String content;
    /** 文章备注 */
    private String remark;
    /** 创建人 */
    private String createUser;
    /** 创建时间 */
    private Date createTime;
    private String createTimeStr;
    /** 修改人 */
    private String updateUser;
    /** 更新时间 */
    private Date updateTime;
    private String updateTimeStr;

    // 保存时用的
    private List<UploadFileModel> attachs = new ArrayList<>();
    
    //回调时候用
    private List<AdeContentAttachment> attachsPage = new ArrayList<>();

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    /***/

    public AdeContent() {
    }

    public AdeContent(String id) {
        this.id = id;
    }

    public AdeContent(String id, String title, String subTitle, String author, String source, String isOutLink,
            String link, Date publishTime, String status, String content, String remark, String createUser,
            Date createTime, String updateUser, Date updateTime) {
        super();
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
        this.author = author;
        this.source = source;
        this.isOutLink = isOutLink;
        this.link = link;
        this.publishTime = publishTime;
        this.status = status;
        this.content = content;
        this.remark = remark;
        this.createUser = createUser;
        this.createTime = createTime;
        this.updateUser = updateUser;
        this.updateTime = updateTime;

    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return this.subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getIsOutLink() {
        return this.isOutLink;
    }

    public void setIsOutLink(String isOutLink) {
        this.isOutLink = isOutLink;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getPublishTime() {
        return this.publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateUser() {
        return this.createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return this.updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPublishTimeStr() {
        return publishTimeStr;
    }

    public void setPublishTimeStr(String publishTimeStr) {
        this.publishTimeStr = publishTimeStr;
    }

    public List<UploadFileModel> getAttachs() {
        return attachs;
    }

    public void setAttachs(List<UploadFileModel> attachs) {
        this.attachs = attachs;
    }
    
    public List<AdeContentAttachment> getAttachsPage() {
        return attachsPage;
    }

    public void setAttachsPage(List<AdeContentAttachment> attachsPage) {
        this.attachsPage = attachsPage;
    }
}
