package com.jijs.framework.model;

public class PageHelper implements java.io.Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4902224755754188177L;

    /**
     * 传递的value信息，这里可以放置id，或者ids等信息
     */
    private String paramValue;

    /**
     * 当前页码
     */
    private int page;

    /**
     * 每页显示记录数
     */
    private int rows;

    /**
     * 排序字段
     */
    private String sort;

    /**
     * asc/desc
     */
    private String order;
    /**
     * 用来显示的数据
     */
    private Object data = null;

    /**
     * 数据总条数
     */
    private int total;

    /**
     * 用来判断获取数据的消息，一般情况下msg为null，表示处理成功。
     */
    private String msg;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
