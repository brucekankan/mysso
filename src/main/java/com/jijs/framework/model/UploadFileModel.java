package com.jijs.framework.model;

/**
 * 
 * @ClassName: UploadFileModel
 * @Description: 封装文件上传的返回值
 * 
 * @date 2016年3月23日 上午10:07:40
 */
public class UploadFileModel {

    /**
     * 文件原始名称
     */
    private String fileNameOld;
    /**
     * 文件相对路径，包括新的文件名
     */
    private String filePath;
    /**
     * form表单中的 名称
     */
    private String fileFormName;

    private long size;

    public UploadFileModel() {
    }

    public UploadFileModel(String fileNameOld, String filePath, String fileFormName) {
        this.fileNameOld = fileNameOld;
        this.filePath = filePath;
        this.fileFormName = fileFormName;
    }

    public UploadFileModel(String fileNameOld, String filePath, String fileFormName, long size) {
        super();
        this.fileNameOld = fileNameOld;
        this.filePath = filePath;
        this.fileFormName = fileFormName;
        this.size = size;
    }

    public String getFileNameOld() {
        return fileNameOld;
    }

    public void setFileNameOld(String fileNameOld) {
        this.fileNameOld = fileNameOld;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileFormName() {
        return fileFormName;
    }

    public void setFileFormName(String fileFormName) {
        this.fileFormName = fileFormName;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "UploadFileModel [fileName_old=" + fileNameOld + ", filePath=" + filePath + ", fileFormName="
                + fileFormName + "]";
    }
}
