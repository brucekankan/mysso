package com.jijs.framework.model.tmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author jijs
 */
@Entity
@Table(name = "ade_dictionary")
public class TadeDictionary implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** id编码 */
    private String id;
    /** 字典名称 */
    private String dictName;
    /** 字典code值 */
    private String dictCode;
    /** 数据字典类型，1：系统字典，8：用户字典 */
    private String dictType;
    /** 字典排序 */
    private Integer dictSort;
    /** 字典描述信息 */
    private String dictDesc;
    /** 是否使用，1：使用，9：不使用 */
    private String dictStatus;
    /** 父节点ID编码 */
    private String dictPid;
    /** 数据字典的值，可以用来组成配置项 */
    private String dictValue;

    /** 默认的构造方法 */
    public TadeDictionary() {
    }

    public TadeDictionary(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TadeDictionary(String id, String dictName, String dictCode, String dictType, Integer dictSort, String dictDesc, String dictStatus, String dictPid, String dictValue) {
        super();
        this.id = id;
        this.dictName = dictName;
        this.dictCode = dictCode;
        this.dictType = dictType;
        this.dictSort = dictSort;
        this.dictDesc = dictDesc;
        this.dictStatus = dictStatus;
        this.dictPid = dictPid;
        this.dictValue = dictValue;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "dict_name", nullable = false, length = 100)
    public String getDictName() {
        return this.dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    @Column(name = "dict_code", nullable = false, length = 32)
    public String getDictCode() {
        return this.dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    @Column(name = "dict_type", nullable = true, length = 1)
    public String getDictType() {
        return this.dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    @Column(name = "dict_sort", nullable = true, length = 11)
    public Integer getDictSort() {
        return this.dictSort;
    }

    public void setDictSort(Integer dictSort) {
        this.dictSort = dictSort;
    }

    @Column(name = "dict_desc", nullable = true, length = 300)
    public String getDictDesc() {
        return this.dictDesc;
    }

    public void setDictDesc(String dictDesc) {
        this.dictDesc = dictDesc;
    }

    @Column(name = "dict_status", nullable = true, length = 2)
    public String getDictStatus() {
        return this.dictStatus;
    }

    public void setDictStatus(String dictStatus) {
        this.dictStatus = dictStatus;
    }

    @Column(name = "dict_pid", nullable = true, length = 32)
    public String getDictPid() {
        return this.dictPid;
    }

    public void setDictPid(String dictPid) {
        this.dictPid = dictPid;
    }

    @Column(name = "dict_value", nullable = true, length = 200)
    public String getDictValue() {
        return this.dictValue;
    }

    public void setDictValue(String dictValue) {
        this.dictValue = dictValue;
    }
}
