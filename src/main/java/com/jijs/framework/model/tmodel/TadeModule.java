package com.jijs.framework.model.tmodel;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author jijs
 */
@Entity
@Table(name = "ade_module")
public class TadeModule implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** ID编码 */
    private String id;
    /** 模块描述 */
    private String moduleDesc;
    /** 模块图标 */
    private String moduleIcon;
    /** 模块名称 */
    private String moduleName;
    /** 模块排序 */
    private Integer moduleSort;
    /** 模块访问路径 */
    private String moduleUrl;
    /** 该模块父节点ID编码 */
    private TadeModule tadeModule;
    /** 权限类型：0、菜单；1、权限 */
    private String moduleType;
    /** 模块维度(前台还是后台)；数据来源为数据字典：ade_module_ground */
    private String moduleGround;
    /**  */
    private Set<TadeModule> tadeModules = new HashSet<TadeModule>(0);
    /**  */
    private Set<TadeRole> tadeRoles = new HashSet<TadeRole>(0);

    /** 默认的构造方法 */
    public TadeModule() {
    }

    public TadeModule(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TadeModule(String id, String moduleDesc, String moduleIcon, String moduleName, Integer moduleSort, String moduleUrl, TadeModule tadeModule, String moduleType, String moduleGround, Set<TadeModule> tadeModules, Set<TadeRole> tadeRoles) {
        super();
        this.id = id;
        this.moduleDesc = moduleDesc;
        this.moduleIcon = moduleIcon;
        this.moduleName = moduleName;
        this.moduleSort = moduleSort;
        this.moduleUrl = moduleUrl;
        this.tadeModule = tadeModule;
        this.moduleType = moduleType;
        this.moduleGround = moduleGround;
        this.tadeModules = tadeModules;
        this.tadeRoles = tadeRoles;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "MODULE_DESC", nullable = true, length = 255)
    public String getModuleDesc() {
        return this.moduleDesc;
    }

    public void setModuleDesc(String moduleDesc) {
        this.moduleDesc = moduleDesc;
    }

    @Column(name = "MODULE_ICON", nullable = true, length = 100)
    public String getModuleIcon() {
        return this.moduleIcon;
    }

    public void setModuleIcon(String moduleIcon) {
        this.moduleIcon = moduleIcon;
    }

    @Column(name = "MODULE_NAME", nullable = false, length = 100)
    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Column(name = "MODULE_SORT", nullable = true, length = 11)
    public Integer getModuleSort() {
        return this.moduleSort;
    }

    public void setModuleSort(Integer moduleSort) {
        this.moduleSort = moduleSort;
    }

    @Column(name = "MODULE_URL", nullable = true, length = 200)
    public String getModuleUrl() {
        return this.moduleUrl;
    }

    public void setModuleUrl(String moduleUrl) {
        this.moduleUrl = moduleUrl;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MODULE_PID")
    public TadeModule getTadeModule() {
        return this.tadeModule;
    }

    public void setTadeModule(TadeModule tadeModule) {
        this.tadeModule = tadeModule;
    }

    @Column(name = "MODULE_TYPE", nullable = false, length = 32)
    public String getModuleType() {
        return this.moduleType;
    }

    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    @Column(name = "MODULE_GROUND", nullable = false, length = 32)
    public String getModuleGround() {
        return this.moduleGround;
    }

    public void setModuleGround(String moduleGround) {
        this.moduleGround = moduleGround;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tadeModule")
    public Set<TadeModule> getTadeModules() {
        return this.tadeModules;
    }

    public void setTadeModules(Set<TadeModule> tadeModules) {
        this.tadeModules = tadeModules;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ade_role_module", joinColumns = { @JoinColumn(name = "MODULE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) })
    public Set<TadeRole> getTadeRoles() {
        return this.tadeRoles;
    }

    public void setTadeRoles(Set<TadeRole> tadeRoles) {
        this.tadeRoles = tadeRoles;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((moduleName == null) ? 0 : moduleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TadeModule other = (TadeModule) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (moduleName == null) {
			if (other.moduleName != null) {
				return false;
			}
		} else if (!moduleName.equals(other.moduleName)) {
			return false;
		}
		return true;
	}
    
    
}
