package com.jijs.framework.model.tmodel;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * @author jijs
 */
@Entity
@Table(name = "ade_role")
public class TadeRole implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** 角色ID编码 */
    private String id;
    /** 角色描述 */
    private String roleDesc;
    /** 角色名称 */
    private String roleName;
    /** 角色排序 */
    private Integer roleSort;
    /** 是否为内置角色，1位内置角色，其它为非内置角色 */
    private Integer roleIsBuildin;
    /**  */
    private Set<TadeModule> tadeModules = new HashSet<TadeModule>(0);
    /**  */
    private Set<TadeUser> tadeUsers = new HashSet<TadeUser>(0);

    /** 默认的构造方法 */
    public TadeRole() {
    }

    public TadeRole(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TadeRole(String id, String roleDesc, String roleName, Integer roleSort, Integer roleIsBuildin, Set<TadeModule> tadeModules, Set<TadeUser> tadeUsers) {
        super();
        this.id = id;
        this.roleDesc = roleDesc;
        this.roleName = roleName;
        this.roleSort = roleSort;
        this.roleIsBuildin = roleIsBuildin;
        this.tadeModules = tadeModules;
        this.tadeUsers = tadeUsers;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "ROLE_DESC", nullable = true, length = 255)
    public String getRoleDesc() {
        return this.roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    @Column(name = "ROLE_NAME", nullable = false, length = 100)
    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Column(name = "ROLE_SORT", nullable = true, length = 11)
    public Integer getRoleSort() {
        return this.roleSort;
    }

    public void setRoleSort(Integer roleSort) {
        this.roleSort = roleSort;
    }

    @Column(name = "ROLE_IS_BUILDIN", nullable = true, length = 1)
    public Integer getRoleIsBuildin() {
        return this.roleIsBuildin;
    }

    public void setRoleIsBuildin(Integer roleIsBuildin) {
        this.roleIsBuildin = roleIsBuildin;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ade_role_module", joinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "MODULE_ID", nullable = false, updatable = false) })
    public Set<TadeModule> getTadeModules() {
        return this.tadeModules;
    }

    public void setTadeModules(Set<TadeModule> tadeModules) {
        this.tadeModules = tadeModules;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ade_user_role", joinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false) })
    public Set<TadeUser> getTadeUsers() {
        return this.tadeUsers;
    }

    public void setTadeUsers(Set<TadeUser> tadeUsers) {
        this.tadeUsers = tadeUsers;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((roleName == null) ? 0 : roleName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TadeRole other = (TadeRole) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (roleName == null) {
			if (other.roleName != null) {
				return false;
			}
		} else if (!roleName.equals(other.roleName)) {
			return false;
		}
		return true;
	}
    
    
}
