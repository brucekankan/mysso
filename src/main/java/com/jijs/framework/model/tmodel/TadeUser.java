package com.jijs.framework.model.tmodel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author jijs
 */
@Entity
@Table(name = "ade_user")
public class TadeUser implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户ID编码 */
    private String id;
    /** 登录名 */
    private String loginName;
    /** 用户名称 */
    private String userName;
    /** 拼音首字母，例如：“公司”首字母为G */
    private String nameLetter;
    /** 密码 */
    private String password;
    /** 性别 */
    private String sex;
    /** 邮箱 */
    private String email;
    /** 电话号码 */
    private String telePhone;
    /** 用户图片 */
    private String userPhoto;
    /** 手机号码 */
    private String mobilePhone;
    /** 用户描述 */
    private String userDesc;
    /** 用户状态，是否使用 1：使用  9 废弃 */
    private String userState;
    /** 创建日期 */
    private Date createDate;
    /** 多个ip用;隔开 */
    private String ip;
    /** 更新时间 */
    private Date updateDate;
    /** 组织机构ID编码 */
    private TadeOrganization tadeOrganization;
    /**  */
    private Set<TadeAdminLogs> tadeAdminLogs = new HashSet<TadeAdminLogs>(0);
    /**  */
    private Set<TadeUserLogs> tadeUserLogs = new HashSet<TadeUserLogs>(0);
    /**  */
    private Set<TadeRole> tadeRoles = new HashSet<TadeRole>(0);

    /** 默认的构造方法 */
    public TadeUser() {
    }

    public TadeUser(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TadeUser(String id, String loginName, String userName, String nameLetter, String password, String sex, String email, String telePhone, String userPhoto, String mobilePhone, String userDesc, String userState, Date createDate, String ip, Date updateDate, TadeOrganization tadeOrganization, Set<TadeAdminLogs> tadeAdminLogs, Set<TadeUserLogs> tadeUserLogs, Set<TadeRole> tadeRoles) {
        super();
        this.id = id;
        this.loginName = loginName;
        this.userName = userName;
        this.nameLetter = nameLetter;
        this.password = password;
        this.sex = sex;
        this.email = email;
        this.telePhone = telePhone;
        this.userPhoto = userPhoto;
        this.mobilePhone = mobilePhone;
        this.userDesc = userDesc;
        this.userState = userState;
        this.createDate = createDate;
        this.ip = ip;
        this.updateDate = updateDate;
        this.tadeOrganization = tadeOrganization;
        this.tadeAdminLogs = tadeAdminLogs;
        this.tadeUserLogs = tadeUserLogs;
        this.tadeRoles = tadeRoles;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "LOGIN_NAME", nullable = false, length = 100)
    public String getLoginName() {
        return this.loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Column(name = "USER_NAME", nullable = true, length = 50)
    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "NAME_LETTER", nullable = true, length = 10)
    public String getNameLetter() {
        return this.nameLetter;
    }

    public void setNameLetter(String nameLetter) {
        this.nameLetter = nameLetter;
    }

    @Column(name = "PASSWORD", nullable = false, length = 100)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "SEX", nullable = true, length = 2)
    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "EMAIL", nullable = true, length = 50)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "TELE_PHONE", nullable = true, length = 30)
    public String getTelePhone() {
        return this.telePhone;
    }

    public void setTelePhone(String telePhone) {
        this.telePhone = telePhone;
    }

    @Column(name = "USER_PHOTO", nullable = true, length = 255)
    public String getUserPhoto() {
        return this.userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    @Column(name = "MOBILE_PHONE", nullable = true, length = 30)
    public String getMobilePhone() {
        return this.mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Column(name = "USER_DESC", nullable = true, length = 255)
    public String getUserDesc() {
        return this.userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    @Column(name = "USER_STATE", nullable = true, length = 2)
    public String getUserState() {
        return this.userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Column(name = "IP", nullable = true)
    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE", nullable = true)
    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_ID")
    public TadeOrganization getTadeOrganization() {
        return this.tadeOrganization;
    }

    public void setTadeOrganization(TadeOrganization tadeOrganization) {
        this.tadeOrganization = tadeOrganization;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tadeUser")
    public Set<TadeAdminLogs> getTadeAdminLogs() {
        return this.tadeAdminLogs;
    }

    public void setTadeAdminLogs(Set<TadeAdminLogs> tadeAdminLogs) {
        this.tadeAdminLogs = tadeAdminLogs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tadeUser")
    public Set<TadeUserLogs> getTadeUserLogs() {
        return this.tadeUserLogs;
    }

    public void setTadeUserLogs(Set<TadeUserLogs> tadeUserLogs) {
        this.tadeUserLogs = tadeUserLogs;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ade_user_role", joinColumns = { @JoinColumn(name = "USER_ID", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID", nullable = false, updatable = false) })
    public Set<TadeRole> getTadeRoles() {
        return this.tadeRoles;
    }

    public void setTadeRoles(Set<TadeRole> tadeRoles) {
        this.tadeRoles = tadeRoles;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((loginName == null) ? 0 : loginName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TadeUser other = (TadeUser) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (loginName == null) {
			if (other.loginName != null) {
				return false;
			}
		} else if (!loginName.equals(other.loginName)) {
			return false;
		}
		return true;
	}
    
    
}
