package com.jijs.framework.model.tmodel;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author jijs
 */
@Entity
@Table(name = "ade_admin_logs")
public class TadeAdminLogs implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** 编码 */
    private String id;
    /** 日志内容 */
    private String content;
    /** 浏览器类型 */
    private String browserType;
    /** 浏览器版本 */
    private String browserVersion;
    /** 操作系统类型 */
    private String osType;
    /** 操作系统版本 */
    private String osVersion;
    /** 模块名称 */
    private String moduleName;
    /** 操作时间 */
    private Date operateTime;
    /** 用户id */
    private TadeUser tadeUser;
    /** ip地址 */
    private String ip;
    /** 日志类型 */
    private String logType;

    /** 默认的构造方法 */
    public TadeAdminLogs() {
    }

    public TadeAdminLogs(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TadeAdminLogs(String id, String content, String browserType, String browserVersion, String osType, String osVersion, String moduleName, Date operateTime, TadeUser tadeUser, String ip, String logType) {
        super();
        this.id = id;
        this.content = content;
        this.browserType = browserType;
        this.browserVersion = browserVersion;
        this.osType = osType;
        this.osVersion = osVersion;
        this.moduleName = moduleName;
        this.operateTime = operateTime;
        this.tadeUser = tadeUser;
        this.ip = ip;
        this.logType = logType;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, length = 36)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "CONTENT", nullable = false, length = 100)
    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "BROWSER_TYPE", nullable = false, length = 36)
    public String getBrowserType() {
        return this.browserType;
    }

    public void setBrowserType(String browserType) {
        this.browserType = browserType;
    }

    @Column(name = "BROWSER_VERSION", nullable = false, length = 36)
    public String getBrowserVersion() {
        return this.browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    @Column(name = "OS_TYPE", nullable = false, length = 36)
    public String getOsType() {
        return this.osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    @Column(name = "OS_VERSION", nullable = false, length = 36)
    public String getOsVersion() {
        return this.osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @Column(name = "MODULE_NAME", nullable = true, length = 300)
    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "OPERATE_TIME", nullable = false)
    public Date getOperateTime() {
        return this.operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    public TadeUser getTadeUser() {
        return this.tadeUser;
    }

    public void setTadeUser(TadeUser tadeUser) {
        this.tadeUser = tadeUser;
    }

    @Column(name = "IP", nullable = true, length = 36)
    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Column(name = "LOG_TYPE", nullable = false, length = 36)
    public String getLogType() {
        return this.logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }
}
