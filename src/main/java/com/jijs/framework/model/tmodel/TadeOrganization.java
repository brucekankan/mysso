package com.jijs.framework.model.tmodel;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author jijs
 */
@Entity
@Table(name = "ade_organization")
public class TadeOrganization implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** id编码 */
    private String id;
    /** 组织机构名称 */
    private String orgName;
    /** 组织机构地址 */
    private String orgAddress;
    /** 组织机构负责人 */
    private String orgLeader;
    /** 联系电话 */
    private String orgPhone;
    /** 详细描述 */
    private String orgDesc;
    /** 上级机构ID编码 */
    private TadeOrganization tadeOrganization;
    /** 是否使用 1：使用  0 废弃 */
    private String orgStatus;
    /**  */
    private Integer orgSort;
    /**  */
    private Set<TadeOrganization> tadeOrganizations = new HashSet<TadeOrganization>(0);
    /**  */
    private Set<TadeUser> tadeUsers = new HashSet<TadeUser>(0);

    /** 默认的构造方法 */
    public TadeOrganization() {
    }

    public TadeOrganization(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TadeOrganization(String id, String orgName, String orgAddress, String orgLeader, String orgPhone, String orgDesc, TadeOrganization tadeOrganization, String orgStatus, Integer orgSort, Set<TadeOrganization> tadeOrganizations, Set<TadeUser> tadeUsers) {
        super();
        this.id = id;
        this.orgName = orgName;
        this.orgAddress = orgAddress;
        this.orgLeader = orgLeader;
        this.orgPhone = orgPhone;
        this.orgDesc = orgDesc;
        this.tadeOrganization = tadeOrganization;
        this.orgStatus = orgStatus;
        this.orgSort = orgSort;
        this.tadeOrganizations = tadeOrganizations;
        this.tadeUsers = tadeUsers;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "ORG_NAME", nullable = true, length = 16)
    public String getOrgName() {
        return this.orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Column(name = "ORG_ADDRESS", nullable = true, length = 64)
    public String getOrgAddress() {
        return this.orgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        this.orgAddress = orgAddress;
    }

    @Column(name = "ORG_LEADER", nullable = true, length = 64)
    public String getOrgLeader() {
        return this.orgLeader;
    }

    public void setOrgLeader(String orgLeader) {
        this.orgLeader = orgLeader;
    }

    @Column(name = "ORG_PHONE", nullable = true, length = 32)
    public String getOrgPhone() {
        return this.orgPhone;
    }

    public void setOrgPhone(String orgPhone) {
        this.orgPhone = orgPhone;
    }

    @Column(name = "ORG_DESC", nullable = true, length = 255)
    public String getOrgDesc() {
        return this.orgDesc;
    }

    public void setOrgDesc(String orgDesc) {
        this.orgDesc = orgDesc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_PID")
    public TadeOrganization getTadeOrganization() {
        return this.tadeOrganization;
    }

    public void setTadeOrganization(TadeOrganization tadeOrganization) {
        this.tadeOrganization = tadeOrganization;
    }

    @Column(name = "ORG_STATUS", nullable = true, length = 2)
    public String getOrgStatus() {
        return this.orgStatus;
    }

    public void setOrgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
    }

    @Column(name = "ORG_SORT", nullable = true, length = 11)
    public Integer getOrgSort() {
        return this.orgSort;
    }

    public void setOrgSort(Integer orgSort) {
        this.orgSort = orgSort;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tadeOrganization")
    public Set<TadeOrganization> getTadeOrganizations() {
        return this.tadeOrganizations;
    }

    public void setTadeOrganizations(Set<TadeOrganization> tadeOrganizations) {
        this.tadeOrganizations = tadeOrganizations;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tadeOrganization")
    public Set<TadeUser> getTadeUsers() {
        return this.tadeUsers;
    }

    public void setTadeUsers(Set<TadeUser> tadeUsers) {
        this.tadeUsers = tadeUsers;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((orgName == null) ? 0 : orgName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TadeOrganization other = (TadeOrganization) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (orgName == null) {
			if (other.orgName != null) {
				return false;
			}
		} else if (!orgName.equals(other.orgName)) {
			return false;
		}
		return true;
	}
    
    
}
