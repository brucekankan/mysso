package com.jijs.ext.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jijs.ext.model.pmodel.AuthSystem;
import com.jijs.ext.service.AuthSystemService;
import com.jijs.framework.CodeConstant;
import com.jijs.framework.controller.BaseController;
import com.jijs.framework.model.EasyUIDataGrid;
import com.jijs.framework.model.JsonResult;
import com.jijs.framework.model.PageHelper;
import com.jijs.framework.util.StringUtil;

/**
 * 认证系统控制器
 * 
 * @author jijs
 * 
 */
@Controller
@RequestMapping("/ext/authSystem")
public class AuthSystemController extends BaseController {

    @Autowired
    private AuthSystemService authSystemService;


    /**
     * 跳转到系统管理页面
     * 
     * @return
     */
    @RequestMapping("/manager")
    public String manager() {
        return "/ext/authSystem/authSystem";
    }

    /**
     * 获取系统数据表格
     * 
     * @param user
     * @return
     */
    @RequestMapping("/dataGrid")
    @ResponseBody
    public EasyUIDataGrid dataGrid(AuthSystem user, PageHelper ph) {
        return authSystemService.dataGrid(user, ph);
    }

    /**
     * 跳转到添加系统页面
     * 
     * @param request
     * @return
     */
    @RequestMapping("/addPage")
    public String addPage(HttpServletRequest request) {
        AuthSystem u = new AuthSystem();
        u.setId(StringUtil.generateUUID());
        request.setAttribute("authSystem", u);
        request.setAttribute("operate", CodeConstant.OPERATE_ADD);
        return "/ext/authSystem/authSystemEdit";
    }

    /**
     * 添加系统
     * 
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public JsonResult add(AuthSystem authSystem) {
        JsonResult j = new JsonResult();
        try {
        	authSystemService.add(authSystem);
            j.setSuccess(true);
            j.setMsg("添加成功！");
            j.setObj(authSystem);
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }

    /**
     * 跳转到系统修改页面
     * 
     * @return
     */
    @RequestMapping("/editPage")
    public String editPage(HttpServletRequest request, String id) {
        AuthSystem u = authSystemService.get(id);
        request.setAttribute("authSystem", u);
        request.setAttribute("operate", CodeConstant.OPERATE_EDIT);
        return "/ext/authSystem/authSystemEdit";
    }

    /**
     * 修改系统
     * 
     * @param authSystem
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public JsonResult edit(AuthSystem authSystem) {
        JsonResult j = new JsonResult();
        try {
        	authSystemService.edit(authSystem);
            j.setSuccess(true);
            j.setMsg("编辑成功！");
            j.setObj(authSystem);
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }

    /**
     * 删除系统
     * 
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResult delete(String id, HttpServletRequest request) {
        JsonResult j = new JsonResult();
        
        authSystemService.delete(id);

        j.setMsg("停用成功！");
        j.setSuccess(true);
        return j;
    }
    
    /**
     * 批量删除系统
     * 
     * @param ids
     *            ('0','1','2')
     * @return
     */
    @RequestMapping("/batchDelete")
    @ResponseBody
    public JsonResult batchDelete(String ids, HttpServletRequest request) {

        JsonResult j = new JsonResult();
        if (ids != null && ids.length() > 0) {
            for (String id : ids.split(",")) {
                if (id != null) {
                    this.delete(id, request);
                }
            }
        }
        j.setMsg("批量删除成功！");
        j.setSuccess(true);
        return j;
    }

    /**
     * 停用系统
     * 
     * @param id
     * @return
     */
    @RequestMapping("/freeze")
    @ResponseBody
    public JsonResult freeze(String id, HttpServletRequest request) {
    	
    	JsonResult j = new JsonResult();
    	if (id != null) {
    		authSystemService.freeze(id);
    	}
    	j.setMsg("停用成功！");
    	j.setSuccess(true);
    	return j;
    }
    
    /**
     * 启用系统
     * 
     * @param id
     * @return
     */
    @RequestMapping("/unFreeze")
    @ResponseBody
    public JsonResult unFreeze(String id, HttpServletRequest request) {
    	
    	JsonResult j = new JsonResult();
    	if (id != null) {
    		authSystemService.unFreeze(id);
    	}
    	j.setMsg("启用成功！");
    	j.setSuccess(true);
    	return j;
    }

}
