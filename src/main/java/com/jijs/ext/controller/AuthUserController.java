package com.jijs.ext.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jijs.ext.model.pmodel.AuthUser;
import com.jijs.ext.service.AuthUserService;
import com.jijs.framework.CodeConstant;
import com.jijs.framework.controller.BaseController;
import com.jijs.framework.model.EasyUIComboTree;
import com.jijs.framework.model.EasyUIDataGrid;
import com.jijs.framework.model.JsonResult;
import com.jijs.framework.model.PageHelper;
import com.jijs.framework.util.StringUtil;
import com.jijs.framework.util.UserUtil;

/**
 * 用户控制器
 * 
 * @author jijs
 * 
 */
@Controller
@RequestMapping("/ext/authUser")
public class AuthUserController extends BaseController {

    @Autowired
    private AuthUserService authUserService;


    /**
     * 跳转到用户管理页面
     * 
     * @return
     */
    @RequestMapping("/manager")
    public String manager() {
        return "/ext/authUser/authUser";
    }

    /**
     * 获取用户数据表格
     * 
     * @param user
     * @return
     */
    @RequestMapping("/dataGrid")
    @ResponseBody
    public EasyUIDataGrid dataGrid(AuthUser user, PageHelper ph) {
        return authUserService.dataGrid(user, ph);
    }

    /**
     * 跳转到添加用户页面
     * 
     * @param request
     * @return
     */
    @RequestMapping("/addPage")
    public String addPage(HttpServletRequest request) {
        AuthUser u = new AuthUser();
        u.setId(StringUtil.generateUUID());
        request.setAttribute("user", u);
        request.setAttribute("operate", CodeConstant.OPERATE_ADD);
        return "/ext/authUser/authUserEdit";
    }

    /**
     * 添加用户
     * 
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public JsonResult add(AuthUser user) {
        JsonResult j = new JsonResult();
        try {
        	authUserService.add(user);
            j.setSuccess(true);
            j.setMsg("添加成功！");
            j.setObj(user);
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }

    /**
     * 跳转到用户修改页面
     * 
     * @return
     */
    @RequestMapping("/editPage")
    public String editPage(HttpServletRequest request, String id) {
        AuthUser u = authUserService.get(id);
        request.setAttribute("user", u);
        request.setAttribute("operate", CodeConstant.OPERATE_EDIT);
        return "/ext/authUser/authUserEdit";
    }

    /**
     * 修改用户
     * 
     * @param user
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public JsonResult edit(AuthUser user) {
        JsonResult j = new JsonResult();
        try {
        	authUserService.edit(user);
            j.setSuccess(true);
            j.setMsg("编辑成功！");
            j.setObj(user);
        } catch (Exception e) {
            j.setMsg(e.getMessage());
        }
        return j;
    }

    /**
     * 删除用户
     * 
     * @param id
     * @return
     */
    @RequestMapping("/freeze")
    @ResponseBody
    public JsonResult freeze(String id, HttpServletRequest request) {

        JsonResult j = new JsonResult();
        authUserService.freeze(id);

        j.setMsg("删除成功！");
        j.setSuccess(true);
        return j;
    }

    /**
     * 恢复用户
     * 
     * @param id
     * @return
     */
    @RequestMapping("/unFreeze")
    @ResponseBody
    public JsonResult unFreeze(String id, HttpServletRequest request) {

        JsonResult j = new JsonResult();
        if (id != null) {
        	authUserService.unFreeze(id);
        }
        j.setMsg("启用成功！");
        j.setSuccess(true);
        return j;
    }

    /**
     * 批量删除用户
     * 
     * @param ids
     *            ('0','1','2')
     * @return
     */
    @RequestMapping("/batchUnFreeze")
    @ResponseBody
    public JsonResult batchUnFreeze(String ids, HttpServletRequest request) {

        JsonResult j = new JsonResult();
        if (ids != null && ids.length() > 0) {
            for (String id : ids.split(",")) {
                if (id != null) {
                    this.freeze(id, request);
                }
            }
        }
        j.setMsg("批量删除成功！");
        j.setSuccess(true);
        return j;
    }
}
