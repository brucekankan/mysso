package com.jijs.ext.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jijs.ext.model.pmodel.AuthSystem;
import com.jijs.ext.model.tmodel.TauthSystem;
import com.jijs.framework.CodeConstant;
import com.jijs.framework.dao.BaseDaoI;
import com.jijs.framework.model.EasyUIDataGrid;
import com.jijs.framework.model.PageHelper;
import com.jijs.framework.util.StringUtil;

/**
 * 用户管理服务类
 * 
 * @author jijs
 *
 */
@Service
public class AuthSystemService {

	@Autowired
	private BaseDaoI<TauthSystem> authSystemDao;




	public EasyUIDataGrid dataGrid(AuthSystem system, PageHelper ph) {
		EasyUIDataGrid dg = new EasyUIDataGrid();
		List<AuthSystem> ul = new ArrayList<AuthSystem>();
		Map<String, Object> params = new HashMap<String, Object>();

		String hql = " from TauthSystem t ";
		List<TauthSystem> l = authSystemDao.find(hql + whereHql(system, params) + orderHql(ph), params, ph.getPage(),
				ph.getRows());
		if (l != null && l.size() > 0) {
			for (TauthSystem t : l) {
				AuthSystem u = new AuthSystem();
				BeanUtils.copyProperties(t, u);

				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(authSystemDao.count("select count(*) " + hql + whereHql(system, params), params));
		return dg;
	}

	private String whereHql(AuthSystem sys, Map<String, Object> params) {
		String hql = "";
		if (sys != null) {
			hql += " where 1=1 ";
			if (StringUtils.isNotEmpty(sys.getSysName())) {
				hql += " and t.sysName like :sysName";
				params.put("sysName", "%%" + sys.getSysName() + "%%");
			}
			if (StringUtils.isNotEmpty(sys.getSysState())) {
				hql += " and t.sysState like :sysState";
				params.put("sysState", "%%" + sys.getSysState() + "%%");
			}
		}
		return hql;
	}

	private String orderHql(PageHelper ph) {

		String orderString = " order by ";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString += " t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	public void add(AuthSystem sys) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sysName", sys.getSysName());
		if (authSystemDao.count("select count(*) from TauthSystem t where t.sysName = :sysName", params) > 0) {
			throw new Exception("系统名已存在！");
		} else {
			TauthSystem s = new TauthSystem();
			BeanUtils.copyProperties(sys, s);
			s.setId(StringUtil.generateUUID());
			s.setCreateDate(new Date());
			s.setUpdateDate(new Date());
			s.setSysState(CodeConstant.STATE_NORMAL);
			authSystemDao.save(s);
		}
	}

	public AuthSystem get(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TauthSystem t = authSystemDao.get("select distinct t from TauthSystem t where t.id = :id", params);
		AuthSystem u = new AuthSystem();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	public void edit(AuthSystem sys) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", sys.getId());
		params.put("sysName", sys.getSysName());
		if (authSystemDao.count("select count(*) from TauthSystem t where t.sysName = :sysName and t.id != :id",
				params) > 0) {
			throw new Exception("系统名已存在！");
		} else {

			TauthSystem u = authSystemDao.get(TauthSystem.class, sys.getId());

			BeanUtils.copyProperties(sys, u, new String[] {"createDate" });
			u.setUpdateDate(new Date());
			authSystemDao.save(u);
		}
	}

	/**
	 * 删除系统
	 * 
	 * @param id
	 */
	public void delete(String id) {
		authSystemDao.delete(new TauthSystem(id));;
	}

	/**
	 * 停用系统
	 * @param id
	 */
	public void freeze(String id) {
		TauthSystem tsys = authSystemDao.get(TauthSystem.class, id);
		tsys.setSysState(CodeConstant.STATE_FREEZE);
		authSystemDao.update(tsys);
	}
	
	/**
	 * 还原系统
	 * 
	 * @param id
	 */
	public void unFreeze(String id) {
		TauthSystem tauthSystem = authSystemDao.get(TauthSystem.class, id);
		tauthSystem.setSysState(CodeConstant.STATE_NORMAL);
		authSystemDao.update(tauthSystem);
	}



}
