package com.jijs.ext.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jijs.ext.model.pmodel.AuthUser;
import com.jijs.ext.model.tmodel.TauthUser;
import com.jijs.framework.CodeConstant;
import com.jijs.framework.dao.BaseDaoI;
import com.jijs.framework.model.EasyUIComboTree;
import com.jijs.framework.model.EasyUIDataGrid;
import com.jijs.framework.model.PageHelper;
import com.jijs.framework.util.MD5Util;
import com.jijs.framework.util.StringUtil;

/**
 * 用户管理服务类
 * 
 * @author jijs
 *
 */
@Service
public class AuthUserService {

	@Autowired
	private BaseDaoI<TauthUser> authUserDao;


	public AuthUser login(AuthUser user) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", user.getLoginName());
		params.put("password", MD5Util.md5(user.getPassword()));
		TauthUser t = authUserDao.get("from TauthUser t where t.loginName = :loginName and t.password = :password", params);
		if (t != null) {
			BeanUtils.copyProperties(t, user);
			return user;
		}
		return null;
	}

	public AuthUser loginByLoginName(String loginName) {
		AuthUser user = new AuthUser();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", loginName);
		TauthUser t = authUserDao.get("from TauthUser t where t.loginName = :loginName", params);
		if (t != null) {
			BeanUtils.copyProperties(t, user);
			return user;
		}
		return null;
	}

	public EasyUIDataGrid dataGrid(AuthUser user, PageHelper ph) {
		EasyUIDataGrid dg = new EasyUIDataGrid();
		List<AuthUser> ul = new ArrayList<AuthUser>();
		Map<String, Object> params = new HashMap<String, Object>();

		String hql = " from TauthUser t ";
		List<TauthUser> l = authUserDao.find(hql + whereHql(user, params) + orderHql(ph), params, ph.getPage(),
				ph.getRows());
		if (l != null && l.size() > 0) {
			for (TauthUser t : l) {
				AuthUser u = new AuthUser();
				BeanUtils.copyProperties(t, u);

				ul.add(u);
			}
		}
		dg.setRows(ul);
		dg.setTotal(authUserDao.count("select count(*) " + hql + whereHql(user, params), params));
		return dg;
	}

	private String whereHql(AuthUser user, Map<String, Object> params) {
		String hql = "";
		if (user != null) {
			hql += " where 1=1 ";
			if (StringUtils.isNotEmpty(user.getLoginName())) {
				hql += " and t.loginName like :loginName";
				params.put("loginName", "%%" + user.getLoginName() + "%%");
			}
			if (StringUtils.isNotEmpty(user.getUserName())) {
				hql += " and t.userName like :userName";
				params.put("userName", "%%" + user.getUserName() + "%%");
			}
			if (StringUtils.isNotEmpty(user.getUserState())) {
				hql += " and t.userState = :userState";
				params.put("userState", user.getUserState());
			}
			if (user.getCreateDateStart() != null) {
				hql += " and t.createDate >= :createDateStart";
				params.put("createDateStart", user.getCreateDateStart());
			}
			if (user.getCreateDateEnd() != null) {
				hql += " and t.createDate <= :createDateEnd";
				params.put("createDateEnd", user.getCreateDateEnd());
			}
			if (user.getUpdateDateStart() != null) {
				hql += " and t.updateDate >= :updateDateStart";
				params.put("updateDateStart", user.getUpdateDateStart());
			}
			if (user.getUpdateDateEnd() != null) {
				hql += " and t.updateDate <= :updateDateEnd";
				params.put("updateDateEnd", user.getUpdateDateEnd());
			}
		}
		return hql;
	}

	private String orderHql(PageHelper ph) {

		String orderString = " order by t.userState asc";
		if (ph.getSort() != null && ph.getOrder() != null) {
			orderString += " ,t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	public void add(AuthUser user) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", user.getLoginName());
		if (authUserDao.count("select count(*) from TauthUser t where t.loginName = :loginName", params) > 0) {
			throw new Exception("登录名已存在！");
		} else {
			TauthUser u = new TauthUser();
			BeanUtils.copyProperties(user, u);
			u.setId(StringUtil.generateUUID());
			u.setCreateDate(new Date());
			u.setPassword(MD5Util.md5(user.getPassword()));
			u.setUserState(CodeConstant.STATE_NORMAL);
			authUserDao.save(u);
		}
	}

	public AuthUser get(String id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TauthUser t = authUserDao.get("select distinct t from TauthUser t where t.id = :id", params);
		AuthUser u = new AuthUser();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	public void edit(AuthUser user) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", user.getId());
		params.put("loginName", user.getLoginName());
		if (authUserDao.count("select count(*) from TauthUser t where t.loginName = :loginName and t.id != :id",
				params) > 0) {
			throw new Exception("登录名已存在！");
		} else {

			TauthUser u = authUserDao.get(TauthUser.class, user.getId());

			BeanUtils.copyProperties(user, u, new String[] { "password", "createDate" });
			u.setUpdateDate(new Date());
			authUserDao.save(u);
		}
	}

	/**
	 * 冻结用户
	 * 
	 * @param id
	 */
	public void freeze(String id) {

		TauthUser tuser = authUserDao.get(TauthUser.class, id);

		tuser.setUserState(CodeConstant.STATE_FREEZE);
		authUserDao.update(tuser);
	}

	/**
	 * 解冻用户
	 * 
	 * @param id
	 */
	public void unFreeze(String id) {
		TauthUser tuser = authUserDao.get(TauthUser.class, id);
		tuser.setUserState(CodeConstant.STATE_NORMAL);
		authUserDao.update(tuser);
	}

	/**
	 * 根据用户登录名获取用户信息
	 * 
	 * @param loginName
	 * @return
	 */
	public AuthUser getUserByLoginName(String loginName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", loginName);
		TauthUser t = authUserDao.get("from TauthUser t where t.loginName = :loginName ", params);
		AuthUser user = new AuthUser();
		if (t != null) {
			BeanUtils.copyProperties(t, user);
			return user;
		}
		return null;
	}

}
