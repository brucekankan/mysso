package com.jijs.ext.model.pmodel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author jijs
 */
public class AuthSystem implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户ID编码 */
    private String id;
    /** 系统名称 */
    private String sysName;
    /** 用户访问路径 */
    private String sysUrl;
    /** 系统状态 */
    private String sysState;
    /** 系统描述 */
    private String sysDesc;
    
    private Date createDate;
    private Date updateDate;

    /** 默认的构造方法 */
    public AuthSystem() {
    }

    public AuthSystem(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public AuthSystem(String id, String sysName, String sysUrl, String sysState, String sysDesc) {
		super();
		this.id = id;
		this.sysName = sysName;
		this.sysUrl = sysUrl;
		this.sysState = sysState;
		this.sysDesc = sysDesc;
	}

    public String getId() {
        return this.id;
    }

	public void setId(String id) {
        this.id = id;
    }


	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getSysUrl() {
		return sysUrl;
	}

	public void setSysUrl(String sysUrl) {
		this.sysUrl = sysUrl;
	}

	public String getSysState() {
		return sysState;
	}

	public void setSysState(String sysState) {
		this.sysState = sysState;
	}

	public String getSysDesc() {
		return sysDesc;
	}

	public void setSysDesc(String sysDesc) {
		this.sysDesc = sysDesc;
	}
	
    public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    
}
