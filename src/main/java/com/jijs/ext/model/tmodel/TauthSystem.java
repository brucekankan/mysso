package com.jijs.ext.model.tmodel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author jijs
 */
@Entity
@Table(name = "auth_system")
public class TauthSystem implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户ID编码 */
    private String id;
    /** 系统名称 */
    private String sysName;
    /** 用户访问路径 */
    private String sysUrl;
    /** 系统状态 */
    private String sysState;
    /** 系统描述 */
    private String sysDesc;
    
    private Date createDate;
    private Date updateDate;

    /** 默认的构造方法 */
    public TauthSystem() {
    }

    public TauthSystem(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TauthSystem(String id, String sysName, String sysUrl, String sysState, String sysDesc) {
		super();
		this.id = id;
		this.sysName = sysName;
		this.sysUrl = sysUrl;
		this.sysState = sysState;
		this.sysDesc = sysDesc;
	}

	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

	public void setId(String id) {
        this.id = id;
    }


    @Column(name = "SYS_NAME")
	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	@Column(name = "SYS_URL")
	public String getSysUrl() {
		return sysUrl;
	}

	public void setSysUrl(String sysUrl) {
		this.sysUrl = sysUrl;
	}

	@Column(name = "SYS_STATE")
	public String getSysState() {
		return sysState;
	}

	public void setSysState(String sysState) {
		this.sysState = sysState;
	}

	@Column(name = "SYS_DESC")
	public String getSysDesc() {
		return sysDesc;
	}

	public void setSysDesc(String sysDesc) {
		this.sysDesc = sysDesc;
	}
	
	@Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "UPDATE_DATE", nullable = true)
    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    
}
