package com.jijs.ext.model.tmodel;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author jijs
 */
@Entity
@Table(name = "auth_user")
public class TauthUser implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /** 用户ID编码 */
    private String id;
    /** 登录名 */
    private String loginName;
    /** 用户名称 */
    private String userName;
    /** 密码 */
    private String password;
    /** 性别 */
    private String sex;
    /** 邮箱 */
    private String email;
    /** 电话号码 */
    private String telePhone;
    /** 用户图片 */
    private String userPhoto;
    /** 手机号码 */
    private String mobilePhone;
    /** 用户描述 */
    private String userDesc;
    /** 用户状态，是否使用 1：使用  9 废弃 */
    private String userState;
    /** 创建日期 */
    private Date createDate;
    /** 更新时间 */
    private Date updateDate;

    /** 默认的构造方法 */
    public TauthUser() {
    }

    public TauthUser(String id) {
        this.id = id;
    }

    /** 带参数的构造方法 */
    public TauthUser(String id, String loginName, String userName, String password, String sex, String email, String telePhone, String userPhoto, String mobilePhone, String userDesc, String userState, Date createDate, Date updateDate) {
        super();
        this.id = id;
        this.loginName = loginName;
        this.userName = userName;
        this.password = password;
        this.sex = sex;
        this.email = email;
        this.telePhone = telePhone;
        this.userPhoto = userPhoto;
        this.mobilePhone = mobilePhone;
        this.userDesc = userDesc;
        this.userState = userState;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, length = 32)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "LOGIN_NAME", nullable = false, length = 100)
    public String getLoginName() {
        return this.loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Column(name = "USER_NAME", nullable = true, length = 50)
    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "PASSWORD", nullable = false, length = 100)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "SEX", nullable = true, length = 2)
    public String getSex() {
        return this.sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Column(name = "EMAIL", nullable = true, length = 50)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "TELE_PHONE", nullable = true, length = 30)
    public String getTelePhone() {
        return this.telePhone;
    }

    public void setTelePhone(String telePhone) {
        this.telePhone = telePhone;
    }

    @Column(name = "USER_PHOTO", nullable = true, length = 255)
    public String getUserPhoto() {
        return this.userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    @Column(name = "MOBILE_PHONE", nullable = true, length = 30)
    public String getMobilePhone() {
        return this.mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Column(name = "USER_DESC", nullable = true, length = 255)
    public String getUserDesc() {
        return this.userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    @Column(name = "USER_STATE", nullable = true, length = 2)
    public String getUserState() {
        return this.userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", nullable = true)
    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_DATE", nullable = true)
    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    
}
