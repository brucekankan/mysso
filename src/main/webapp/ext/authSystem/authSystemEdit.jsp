<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
  $(function() {
    $('#form').form({
      url : '${pageContext.request.contextPath}/ext/authSystem/${operate}',
      onSubmit : function() {
        parent.$.messager.progress({
          title : '提示',
          text : '数据处理中，请稍后....'
        });
        var isValid = $(this).form('validate');
        if (!isValid) {
          parent.$.messager.progress('close');
        }
        return isValid;
      },
      success : function(result) {
        parent.$.messager.progress('close');
        result = $.parseJSON(result);
        if (result.success) {
          parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为user.jsp页面预定义好了
          parent.$.modalDialog.handler.dialog('close');
          parent.$.messager.show({
            title : '成功',
            msg : result.msg
          });
        } else {
          parent.$.messager.alert('错误', result.msg, 'error');
        }
      }
    });
    parent.$.messager.progress('close');
  });
</script>

<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;" class="ade-gray">
		<form id="form" method="post">
			<input name="id" type="hidden" value="${authSystem.id}" />
			<table class="ade-table">
				<tr>
					<th>系统名称1：</th>
					<td><c:if test="${operate=='edit'}">
							<input name="sysName" type="text" placeholder="请输入系统名称" class="easyui-validatebox" style="height: 30px;"
								data-options="required:true" value="${authSystem.sysName}" readonly="readonly" />
						</c:if> <c:if test="${operate=='add'}">
							<input name="sysName" type="text" placeholder="请输入系统名称" class="easyui-validatebox" style="height: 30px;"
								data-options="required:true" value="" />
						</c:if></td>
				</tr>
				<tr>
					<th>系统访问地址：</th>
					<td><input name="sysUrl" type="text" placeholder="请输入系统访问地址" class="easyui-validatebox" style="height: 30px;"
						value="${authSystem.sysUrl}" /></td>
				</tr>
				<tr>
					<th>系统状态：</th>
					<td><input name="sysState" type="radio" value="1"
						<c:if test="${authSystem.sysState == '1'}">checked="checked"</c:if> />正常<input name="sysState" type="radio" value="9"
						<c:if test="${authSystem.sysState != '1'}">checked="checked"</c:if> />冻结</td>
				</tr>
				<tr>
					<th>备注：</th>
					<td><textarea name="sysDesc" style="width: 395px; height: 100px;">${authSystem.sysDesc}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
</div>
