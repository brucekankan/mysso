<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>用户管理</title>
<%@ include file="/WEB-INF/inc/adeInc.jsp"%>
<c:if test="${fn:contains(LOGIN_USER.moduleList, '/ext/authSystem/editPage')}">
	<script type="text/javascript">
    $.canEdit = true;
  </script>
</c:if>
<c:if test="${fn:contains(LOGIN_USER.moduleList, '/ext/authSystem/delete')}">
	<script type="text/javascript">
    $.canDelete = true;
  </script>
</c:if>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ext/authSystem/authSystem.js">
</script>
<script type="text/javascript">
  var currentUserId = '${LOGIN_USER.id}';
</script>
</head>
<body>

	<div id="toolbar" style="padding: 5px; height: auto">
		<div>
			<form id="searchForm" style="margin: 5px;">
				<table class="ade-table">
					<tr>
						<th>系统名:</th>
						<td><input name="sysName" class="easyui-textbox" data-options="width:150,prompt: '登录名',height:30" /></td>
						<th>系统状态:</th>
						<td><select id="cc" class="easyui-combobox" name="sysState" style="width: 150px;"
							data-options="editable:false,multiple:false,height:30">
								<option value="">全部</option>
								<option value="1">正常</option>
								<option value="9">停用</option>
						</select></td>
						<td colspan="4"><a href="javascript:void(0);" class="easyui-linkbutton ade-button-1"
							data-options="plain:true" onclick="searchFun();"><icon:icon name="search" />&nbsp;查询</a><a
							href="javascript:void(0);" class="easyui-linkbutton ade-button-2" data-options="plain:true" onclick="cleanFun();"><icon:icon
									name="clean" />&nbsp;清空条件</a><a href="javascript:void(0);" class="easyui-linkbutton ade-button-2"
							data-options="plain:true" onclick="moreFun();"><icon:icon name="more" />&nbsp;更多</a></td>
					</tr>
				</table>
			</form>

			<hr />

			<c:if test="${fn:contains(LOGIN_USER.moduleList, '/ext/authSystem/addPage')}">
				<a onclick="addFun();" href="javascript:void(0);" class="easyui-linkbutton ade-button-1" data-options="plain:true"><icon:icon
						name="add" />&nbsp;添加</a>
			</c:if>

			<c:if test="${fn:contains(LOGIN_USER.moduleList, '/ext/authSystem/delete')}">
				<a onclick="batchDeleteFun();" href="javascript:void(0);" class="easyui-linkbutton ade-button-2"
					data-options="plain:true"><icon:icon name="stop" />&nbsp;批量删除</a>
			</c:if>
		</div>
	</div>
	<table id="dataGrid"></table>
</body>
</html>